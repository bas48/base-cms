DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
defaultFile="$DIR/default/.gitlab-ci.yml.default"
currentFile="$DIR/../.gitlab-ci.yml"

# Reset file
if [[ $defaultFile ]]; then
rm -rf $currentFile
cp -i $defaultFile $currentFile
fi

while : ; do
    read -p "Enter project name: " PROJECT_NAME
    [[ $PROJECT_NAME = "" ]] || break
done

    read -p "Enter project type (cms/web): " PROJECT_TYPE
while  [[ $PROJECT_TYPE != "cms" && $PROJECT_TYPE != "web" ]]
do
    printf "Project type must be cms or web\n"
    read -p "Enter project type (cms/web): " PROJECT_TYPE
done

while : ; do
    read -p "Enter stage domain: " STAGE_DOMAIN
    [[ $STAGE_DOMAIN = "" ]] || break
done

sed -i "" "s/{{PROJECT_NAME}}/$PROJECT_NAME/g; s/{{PROJECT_TYPE}}/$PROJECT_TYPE/g; s/{{STAGE_DOMAIN}}/$STAGE_DOMAIN/g" $currentFile
