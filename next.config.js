const path = require('path');
const withCSS = require('@zeit/next-css');
const withNextAntdLess = require('./next-antd-less.config');
require('dotenv').config();
require('dotenv').config({ path: path.join(__dirname, '.env.default') });

const publicConfigs = {};
for (const key of Object.keys(process.env)) {
  if (key.indexOf('REACT_') === 0) {
    publicConfigs[key] = process.env[key];
  }
}

module.exports = withNextAntdLess(withCSS({
  webpack: (config) => {
    const newConfig = { ...config };
    newConfig.plugins = config.plugins || [];
    newConfig.plugins = [...config.plugins];
    newConfig.module.rules.push({
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      use: [
        {
          loader: 'babel-loader',
        },
        {
          loader: '@svgr/webpack',
          options: {
            babel: false,
            icon: true,
          },
        },
      ],
    });
    return newConfig;
  },
  pageExtension: ['tsx'],
  serverRuntimeConfig: { // Will only be available on the server side
    ...process.env,
  },
  publicRuntimeConfig: { // Will only be available on the client side
    ...publicConfigs,
  },
  pageExtensions: ['jsx', 'tsx'],
}));
