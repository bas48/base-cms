# Set Up

**Environment Setup:**
1. Copy **.env.default** file
2. Change the file name to **.env**
3. Update the .env parameters as project required
4. Run **yarn dev** to check can the frontend run successfully

**First Time Run Frontend**
```bash
yarn #Install library
yarn dev #Start frontend
 ```

# Command
|Command|Description|
| ------------ | ------------ |
|yarn|Install library|
|yarn dev|Run project with hot reload|
|yarn build|Build project|
|yarn start|Start project|
|yarn lint|Error checking|
|yarn init-stage|Setup staging deployment|

# Hook
#### Common Hook

Provide **style**, **translation** and **store** handlers
Example
```javascript
import useCommon from '@utils/hooks/common';
import styles from './styles';

interface Props {
}

const Example = (props: Props) => {
  const { classes, theme, t, language } = useCommon({
    withStyles: styles,
    withTranslation: ['translation'],
    observers: ['language'],
  });

  return (
    <div className={classes.example}>
      {t('example')}
    </div>
  );
};

export default Example;
```

|Key|Description|
|------------|------------|
|withStyles|Insert style with theme, useCommon will provide classes function|
|withTranslation|Insert translation handler, useCommon will provide t function|
|observers|Insert store useCommon will provide store function|

#### Request Hook

Provide the **loading** state when call the request

```javascript
import useRequest from '@utils/hooks/request';

const { loading, run } = useRequest(async (apiData) => {
  await testApi.apiFunction(apiData);
}, { showError: true });

run({ apiParam });
```

|Key|Description|
|------------|------------|
|loading|Loading state when calling api function|
|run|Trigger function to call api|
|showError|Display error, if api function response error status|
|successMessage|Display success message after api sent|

# Add Page
1. Add page in **/src/pages**
2. Page url equals to folder or file name.

#### Add Components
1. Add component in **/src/components**

# Translation
    <div> {t('example_translation')} </div>
- Edit the translation file in **/server/i18n/resources**
- Restart the client after edited the file in order to see the change during development
