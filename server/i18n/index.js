/* eslint-disable @typescript-eslint/no-var-requires */
const fs = require('fs');
const path = require('path');
const axios = require('axios');
require('dotenv').config();
require('dotenv').config({ path: path.join(__dirname, '.env.default') });

const localResourcePath = path.join(__dirname, 'resources');

const localFiles = Object.assign({}, ...fs.readdirSync(localResourcePath).filter((file) => (file.indexOf('.') !== 0) && (file.slice(-3) === '.js')).map((file) => {
  // eslint-disable-next-line global-require, import/no-dynamic-require
  const localFile = require(path.join(localResourcePath, file));
  return {
    [file.split('.')[0]]: localFile,
  };
}));

const client = axios.create({
  baseURL: process.env.REACT_URL,
});

const initTranslationSource = async () => {
  await client.post('/v1/translation/source', {
    platform: process.env.REACT_WEB_TYPE,
    source: localFiles,
  });
};

module.exports = { initTranslationSource };
