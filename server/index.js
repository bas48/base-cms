/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
require('dotenv').config();
require('dotenv').config({ path: path.join(__dirname, '.env.default') });

const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const next = require('next');
const Router = require('koa-router');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev, dir: path.resolve(__dirname, '../src') });
const nextHandler = nextApp.getRequestHandler();
const { initTranslationSource } = require('./i18n');

nextApp.prepare().then(() => {
  const server = new Koa();
  const router = new Router();
  initTranslationSource();
  server.use(bodyParser());

  router.get('(.*)', async (ctx) => {
    ctx.req.ctx = ctx;
    await nextHandler(ctx.req, ctx.res);
    ctx.respond = false;
  });

  // eslint-disable-next-line no-shadow
  server.use(async (ctx, next) => {
    ctx.res.statusCode = 200;
    await next();
  });

  server.use(router.routes());
  server.listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`);
  });
});
