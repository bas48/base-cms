const styles = {
  editor: {
    '& .ck-content': {
      maxHeight: 400,
    },
  },
};

export default styles;
