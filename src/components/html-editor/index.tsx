/* eslint-disable global-require */
import { useState, useEffect } from 'react';
import classnames from 'classnames';
import { Input } from 'antd';
import useCommon from '@utils/hooks/common';
import fileApi from '@utils/api/file.api';
import getConfig from 'next/config';
// import ImageResize from '@ckeditor/ckeditor5-image/src/imageresize';
import styles from './styles';

const { publicRuntimeConfig } = getConfig();

const { REACT_URL } = publicRuntimeConfig;

const { TextArea } = Input;

interface PropsType {
  className?: any;
  value?: string;
  onChange?: Function;
  disabled?: boolean;
}

class UploadAdapter {
  loader;

  constructor(loader) {
    this.loader = loader;
  }

  upload() {
    return new Promise((resolve, reject) => {
      this.loader.file.then(async (file) => {
        try {
          const filePath = await fileApi.uploadHtmlImage(file);
          console.log('filePath:', filePath);
          resolve({
            default: `${REACT_URL}${filePath}`,
          });
        } catch (error) {
          reject(error);
        }
      });
    });
  }
}

const HtmlEditor = (props: PropsType) => {
  const { className, value, onChange, disabled } = props;
  const { classes } = useCommon({
    withStyles: styles,
  });

  const [editorSetting, setEditorSetting] = useState(null);

  useEffect(() => {
    if (window) {
      const { CKEditor } = require('@ckeditor/ckeditor5-react');
      const ClassicEditor = require('@ckeditor/ckeditor5-build-classic');
      // const ImageResize = require('@ckeditor/ckeditor5-image/src/imageresize');
      setEditorSetting({
        CKEditor,
        ClassicEditor,
        // ImageResize,
      });
    }
  }, []);

  if (!editorSetting) {
    return (
      <TextArea />
    );
  }

  const uploadAdapterPlugin = (editor) => {
    // eslint-disable-next-line no-param-reassign
    editor.plugins.get('FileRepository').createUploadAdapter = (loader) => new UploadAdapter(loader);
  };

  const { CKEditor, ClassicEditor } = editorSetting;

  return (
    <div className={classnames(classes.editor, className)}>
      <CKEditor
        editor={ClassicEditor}
        config={{
          extraPlugins: [uploadAdapterPlugin],
          // plugins: [ImageResize],
        }}
        data={value}
        onChange={(event, editor) => {
          const data = editor.getData();
          console.log('On change:', data);
          if (onChange && !disabled) {
            onChange(data);
          }
        }}
      />
    </div>
  );
};

export default HtmlEditor;
