const styles = {
  item: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignContent: 'center',
    '& >span': {
      marginRight: 20,
      minWidth: 100,
      textAlign: 'center',
    },
    '& .ant-select': {
      width: 150,
    },
  },
};

export default styles;
