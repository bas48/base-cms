/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-props-no-spreading */
import { useState, useEffect } from 'react';
import { Select, Spin } from 'antd';
import commonApi from '@utils/api/common.api';
import useCommon from '@utils/hooks/common';
import styles from './styles';

const { Option } = Select;

export type PropsType = {
  dataPath?: string;
  dataSource?: any[];
  afterFetchSource?: (response: any) => any[];
  defineValue: (option: any) => any;
  defineLabel: (option: any) => string;
} & {
  [prop: string]: any;
}

const getReturnOption = (mode, options, returnValue) => {
  let returnOption;
  if (mode && mode === 'multiple') {
    if (returnValue.length) {
      returnOption = [];
      for (const id of returnValue) {
        returnOption.push(options[id].item);
      }
    } else {
      // eslint-disable-next-line
      returnValue = undefined;
    }
  } else {
    returnOption = returnValue ? options[returnValue].item : undefined;
  }
  return returnOption;
};

const fetchOptions = async (props: PropsType) => {
  const { dataPath, dataSource, afterFetchSource, defineValue, defineLabel } = props;
  let sources = dataSource || await commonApi.getSelectorItems(dataPath);
  if (afterFetchSource) {
    sources = await afterFetchSource(sources);
  }
  const options = {};
  for (const item of sources) {
    const optionValue = defineValue(item);
    const optionLabel = defineLabel ? defineLabel(item) : optionValue;
    options[optionValue] = {
      label: optionLabel,
      item,
    };
  }
  return options;
};

const valueTypeHandler = (value) => {
  if (value) {
    if (Array.isArray(value)) {
      if (value.length) {
        return value.map((v) => String(v));
      }
      return undefined;
    }
    return String(value);
  }
  return value;
};

const DataSelector = (props: PropsType) => {
  const { value, mode, onDefault, className, onChange } = props;
  const [options, setOption] = useState(null);
  useEffect(() => {
    const fetchData = async () => {
      if (!options) {
        const fetchedOptions = await fetchOptions(props);
        setOption(fetchedOptions);
      }
    };
    fetchData();
    if (options && value && onDefault) {
      const defaultOption = getReturnOption(mode, options, value);
      onDefault(value, defaultOption);
    }
  }, [options]);
  return (
    options && Object.keys(options).length
      ? (
        <Select
          className={className}
          value={valueTypeHandler(value)}
          mode={mode}
          onChange={(returnValue: any) => {
            const returnOption = getReturnOption(mode, options, returnValue);
            console.log('returnValue: ', returnValue);
            console.log('onchange: ', returnOption);
            if (onChange) {
              onChange(returnValue, returnOption);
            }
          }}
          filterOption={(input, option) => {
            const selectedKey = Object.keys(options).find((optionKey) => optionKey === option.key);
            return options[selectedKey].label.toLowerCase().includes(input.toLowerCase());
          }}
        >
          {
            Object.keys(options).map((index) => (
              <Option key={index} value={index}>{options[index].label}</Option>
            ))
          }
        </Select>
      ) : <Spin />
  );
};

export default DataSelector;
