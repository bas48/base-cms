const styles = (theme) => ({
  layout: {
    height: '100vh',
  },
  content: {
    background: theme.contentBackground,
    width: '100%',
    height: '100%',
  },
  loadingScreen: {
    zIndex: 10,
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    width: '100vw',
    height: '100vh',
    position: 'absolute',
    top: 0,
    left: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
