import getConfig from 'next/config';
import { Layout } from 'antd';
import useCommon from '@utils/hooks/common';
import DataSelector from '@components/data-selector';
import UserMenu from '@components/layout/user-menu';
import styles from './styles';

const { publicRuntimeConfig } = getConfig();
const { Header } = Layout;

const HeaderBar = () => {
  const { session, language, classes } = useCommon({
    withStyles: styles,
    observers: ['session', 'language'],
  });
  const { currentLanguage } = language;
  return (
    <Header>
      <div className={classes.header}>
        <img alt="logo" className={classes.logo} src={`${publicRuntimeConfig.REACT_URL}/resources/public/system/cms/logo.png`} />
        <div className={classes.space} />
        {
          session.isLogin
            ? <UserMenu /> : null
        }
        <DataSelector
          dataPath="/v1/language"
          defineLabel={(option) => option.name}
          defineValue={(option) => option.id}
          className={classes.language}
          value={currentLanguage}
          onChange={(value) => {
            language.setLanguage(value);
            window.location.reload(false);
          }}
        />
      </div>
    </Header>
  );
};

export default HeaderBar;
