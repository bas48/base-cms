const styles = {
  header: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  logo: {
    height: '70%',
    objectFit: 'contain',
  },
  space: {
    flex: 1,
  },
  language: {
    marginLeft: 10,
  },
};

export default styles;
