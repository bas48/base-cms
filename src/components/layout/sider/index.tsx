import { Layout, Menu } from 'antd';
import Router from 'next/router';
import useCommon from '@utils/hooks/common';
import styles from './styles';
import menus from './menu';

const { SubMenu, Item } = Menu;
const { Sider } = Layout;

const SiderBar = () => {
  const { t, permission } = useCommon({
    withStyles: styles,
    withTranslation: ['sider'],
    observers: ['permission'],
  });

  // const [collapsed, setCollapsed] = useState(false);

  const getMenuItem = (mainPath, data) => {
    const { requiredPermissions, icon, label, path, subMenus } = data;
    if (requiredPermissions) {
      const checkResult = permission.comparePermission(requiredPermissions);
      if (!checkResult.success) {
        console.log('Need Permission:', checkResult.needPermission);
        return null;
      }
    }
    if (subMenus) {
      return (
        <SubMenu
          key={`${mainPath}${path}`}
          title={(
            <span>
              {icon || null}
              <span>{t(label)}</span>
            </span>
          )}
        >
          {
            subMenus.map((menu) => (
              getMenuItem(`${mainPath}${path}`, menu)
            ))
          }
        </SubMenu>
      );
    }
    return (
      <Item key={`${mainPath}${path}`} onClick={() => Router.push(`${mainPath}${path}`)}>
        {icon || null}
        <span>{t(label)}</span>
      </Item>
    );
  };
  return (
    <Sider
      width="fit-content"
      style={{
        background: 'white',
      }}
      // collapsible
      // collapsed={collapsed}
      // onCollapse={() => setCollapsed(!collapsed)}
    >
      <Menu
        mode="inline"
      >
        {
          menus.map((menu) => (
            getMenuItem('', menu)
          ))
        }
      </Menu>
    </Sider>
  );
};

export default SiderBar;
