import {
  UserOutlined,
  TeamOutlined,
  KeyOutlined,
  ApiOutlined,
  GlobalOutlined,
  MessageOutlined,
  NotificationOutlined,
  CodeOutlined,
  NumberOutlined,
  ForkOutlined,
  CheckSquareOutlined,
} from '@ant-design/icons';

const menu = [
  {
    path: '/user-management',
    label: 'user_management',
    icon: <UserOutlined />,
    requiredPermissions: {
      user: 'read',
    },
  },
  {
    path: '/role-management',
    label: 'role_management',
    icon: <TeamOutlined />,
    requiredPermissions: {
      role: 'read',
    },
  },
  {
    path: '/permission-management',
    label: 'permission_management',
    icon: <KeyOutlined />,
    requiredPermissions: {
      permission: 'read',
    },
  },
  {
    path: '/resource-management',
    label: 'resource_management',
    icon: <ApiOutlined />,
    requiredPermissions: {
      resource: 'read',
    },
  },
  {
    path: '/translation-management',
    label: 'translation_management',
    icon: <GlobalOutlined />,
    requiredPermissions: {
      translation: 'read',
    },
  },
  {
    path: '/message-management',
    label: 'message_management',
    icon: <MessageOutlined />,
    requiredPermissions: {
      message: 'read',
    },
  },
  {
    path: '/notification-management',
    label: 'notification_management',
    icon: <NotificationOutlined />,
    requiredPermissions: {
      notification: 'read',
    },
  },
  {
    path: '/query-management',
    label: 'query_management',
    icon: <CodeOutlined />,
    requiredPermissions: {
      query: 'read',
    },
  },
  {
    path: '/constant-management',
    label: 'constant_management',
    icon: <NumberOutlined />,
    requiredPermissions: {
      constant: 'read',
    },
  },
  {
    path: '/socket-test',
    label: 'socket:socket_test',
    icon: <ForkOutlined />,
  },
  {
    path: '/test',
    label: 'test',
    icon: <CheckSquareOutlined />,
  },
];

export default menu;
