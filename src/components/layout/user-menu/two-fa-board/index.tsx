/* eslint-disable jsx-a11y/no-static-element-interactions */
import { useEffect, useState } from 'react';
import { Modal } from 'antd';
import QRCode from 'qrcode.react';
import useCommon from '@utils/hooks/common';
import useRequest from '@utils/hooks/request';
import userApi from '@utils/api/user.api';
import CodeInput from '@components/verification/code-input';
import styles from './styles';

interface PropsType {
  visible: boolean;
  onClose: Function;
}

const TwoFABoard = (props: PropsType) => {
  const { t, classes, user } = useCommon({
    withTranslation: ['two-fa-board'],
    withStyles: styles,
    observers: ['user'],
  });
  const { visible, onClose } = props;
  const { info } = user;
  const { twoFAEnable } = info;
  const [token, setToken] = useState(null);
  const [qrCode, setQrCode] = useState(null);
  const { loading, run: toggleTwoFA } = useRequest(async () => {
    await user.updateTwoFactor(token);
    setQrCode(null);
    onClose();
  }, { showError: true, successMessage: t('translation:success') });

  useEffect(() => {
    const getQrCode = async () => {
      const response = await userApi.getTwoFAQrCode();
      setQrCode(response.qrCode);
    };
    if (!twoFAEnable && !qrCode) {
      getQrCode();
    }
  });

  return (
    <Modal
      title={t(
        twoFAEnable ? 'disable_field' : 'enable_field',
        { field: t('two_factor') },
      )}
      destroyOnClose
      visible={visible}
      onOk={() => toggleTwoFA()}
      onCancel={() => {
        setToken(null);
        setQrCode(null);
        if (onClose) {
          onClose();
        }
      }}
      okButtonProps={{ disabled: !token, loading }}
    >
      <div className={classes.two_factor_content}>
        {
          qrCode
            ? <QRCode size={200} value={qrCode} /> : null
        }
        <CodeInput
          length={6}
          onChange={(code) => setToken(code)}
          onKeyDown={(keyCode) => { if (keyCode === 13) toggleTwoFA(); }}
        />
      </div>
    </Modal>
  );
};

export default TwoFABoard;
