const styles = {
  two_factor_content: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    '& input': {
      marginTop: 10,
    },
  },
};

export default styles;
