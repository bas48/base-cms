/* eslint-disable jsx-a11y/no-static-element-interactions */
import { Modal, Form, Input, Button } from 'antd';
import useCommon from '@utils/hooks/common';
import useRequest from '@utils/hooks/request';
import userApi from '@utils/api/user.api';

interface PropsType {
  visible: boolean;
  onClose: Function;
}

const ChangePasswordBoard = (props: PropsType) => {
  const { t } = useCommon({
    withTranslation: ['change-password-board'],
  });
  const { visible, onClose } = props;
  const [form] = Form.useForm();
  const { loading, run: changePassword } = useRequest(async (data) => {
    await userApi.changePersonalInfo(data);
    form.resetFields();
    onClose();
  }, { showError: true, successMessage: t('translation:success') });

  return (
    <Modal
      title={t('change_password')}
      destroyOnClose
      visible={visible}
      onCancel={() => {
        if (onClose) {
          onClose();
        }
      }}
      footer={null}
    >
      <Form
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={({ oldPassword, newPassword }) => changePassword({ oldPassword, newPassword })}
        form={form}
      >
        <Form.Item
          name="oldPassword"
          label={t('old_password')}
          rules={[
            {
              required: true,
              message: t('error:please_confirm', { field: t('old_password') }),
            },
            {
              validator: (rule, value, callback) => {
                if (value) {
                  form.validateFields(['confirmPassword']);
                }
                callback();
              },
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          name="newPassword"
          label={t('new_password')}
          rules={[
            {
              required: true,
              message: t('error:please_confirm', { field: t('new_password') }),
            },
            {
              validator: (rule, value, callback) => {
                if (value) {
                  form.validateFields(['confirmPassword']);
                }
                callback();
              },
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          name="confirmPassword"
          label={t('confirm_password')}
          rules={[
            {
              required: true,
              message: t('error:please_confirm', { field: t('confirm_password') }),
            },
            {
              validator: (rule, value, callback) => {
                if (value && value !== form.getFieldValue('newPassword')) {
                  callback(t('error:password_inconsistent'));
                } else if (form.getFieldValue('password') && !value && !true) {
                  callback(t('error:please_confirm', { field: t('password') }));
                } else {
                  callback();
                }
              },
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" loading={loading}>
            {t('submit')}
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default ChangePasswordBoard;
