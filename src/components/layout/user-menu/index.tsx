/* eslint-disable jsx-a11y/no-static-element-interactions */
import { useState } from 'react';
import { Menu, Dropdown, Button, Row, Col } from 'antd';
import { UserOutlined, SecurityScanOutlined, KeyOutlined, LogoutOutlined } from '@ant-design/icons';
import Router from 'next/router';
import useCommon from '@utils/hooks/common';
import TwoFABoard from '@components/layout/user-menu/two-fa-board';
import ChangePasswordBoard from '@components/layout/user-menu/change-password-board';
import styles from './styles';

const UserMenu = () => {
  const { t, user, userName } = useCommon({
    withStyles: styles,
    withTranslation: ['user-menu'],
    observers: ['user', (store) => () => ({
      userName: store.user.info.name,
    })],
  });
  const { info } = user;
  const { twoFAEnable } = info;
  const [showTwoFA, setShowTwoFA] = useState(false);
  const [showChangePassword, setShowChangePassword] = useState(false);
  const menuItems = [
    {
      icon: <SecurityScanOutlined />,
      label: t(
        twoFAEnable ? 'disable_field' : 'enable_field',
        { field: t('two_factor') },
      ),
      onClick: async () => setShowTwoFA(true),
    },
    {
      icon: <KeyOutlined />,
      label: t('change_password'),
      onClick: async () => setShowChangePassword(true),
    },
    {
      icon: <LogoutOutlined />,
      label: t('logout'),
      onClick: async () => {
        await user.logout();
        Router.push('/login');
      },
    },
  ];

  return (
    <>
      <Dropdown overlay={() => (
        <Menu onClick={({ key }) => menuItems[key].onClick()}>
          {
            menuItems.map((item, key) => {
              const { icon, label } = item;
              return (
                <Menu.Item key={key} icon={icon}>
                  {label}
                </Menu.Item>
              );
            })
          }
        </Menu>
      )}
      >
        <Button>
          <Row align="middle" gutter={10}>
            <Col><UserOutlined /></Col>
            <Col>
              {t('hello_user', { user: userName })}
            </Col>
          </Row>
        </Button>
      </Dropdown>
      <TwoFABoard visible={showTwoFA} onClose={() => setShowTwoFA(false)} />
      <ChangePasswordBoard visible={showChangePassword} onClose={() => setShowChangePassword(false)} />
    </>
  );
};

export default UserMenu;
