import { useEffect, useState } from 'react';
import { Layout, notification, Spin } from 'antd';
import Router, { useRouter } from 'next/router';
import { autorun } from 'mobx';
import useCommon from '@utils/hooks/common';
import VerificationBoard from '@components/verification/verification-board';
import styles from './styles';
import Header from './header';
import SiderBar from './sider';

const { Content } = Layout;

const notify = (message) => {
  const { type, content } = message;
  notification[type]({
    message: content,
  });
};

const LayoutBase = ({ children }: any) => {
  const { classes, session, system } = useCommon({
    withStyles: styles,
    withTranslation: null,
    observers: ['session', 'system'],
  });
  const router = useRouter();
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    autorun(() => {
      if (!session.isLogin && !router.asPath.includes('login')) {
        Router.push('/login');
      }
      if (system.message) {
        notify(system.message);
        system.clearMessage(null);
      }
    });
    setIsMounted(true);
  }, []);
  if (!isMounted) {
    return <Spin />;
  }
  return (
    <Layout className={classes.layout}>
      <Header />
      <Layout>
        {
          session.isLogin ? <SiderBar /> : null
        }
        <Layout>
          <Content className={classes.content}>
            {children}
          </Content>
        </Layout>
      </Layout>
      {/* {
        system.isLoading
          ? (
            <div className={classes.loadingScreen}>
              <Spin size="large" tip={system.loadingList[0].message} />
            </div>
          ) : null
      } */}
      <VerificationBoard />
    </Layout>
  );
};

export default LayoutBase;
