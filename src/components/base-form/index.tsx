import { useState, useEffect } from 'react';
import _ from 'lodash';
import { Button, Form, Tabs } from 'antd';
import { FormItemProps } from 'antd/lib/form/FormItem';
import { FormInstance, FormProps } from 'antd/lib/form';
import useCommon from '@utils/hooks/common';
import { convertTrueObjToObj } from '@utils/other/object-convertor';
import styles from './styles';

const { TabPane } = Tabs;

export interface FormItem extends FormItemProps{
  isTranslation?: boolean;
  render: Function;
}

interface PropsType {
  customFooter?: Function;
  onClear?: Function;
  loading?: boolean;
  initialValues?: any;
  // eslint-disable-next-line no-unused-vars
  formItems: (form: FormInstance) => FormItem[],
  subForms?: Function,
  onSubmit: Function,
  formProps?: FormProps;
}

const BaseForm = (props: PropsType) => {
  const { t, classes, language } = useCommon({
    withTranslation: ['base-form'],
    withStyles: styles,
    observers: ['language'],
  });
  const [form] = Form.useForm();
  const { customFooter, formProps = {}, onClear, loading, formItems, initialValues, subForms, onSubmit } = props;
  const [state, setState] = useState({
    generalItems: [],
    translationItems: [],
    errorTranslation: null,
    generalError: null,
    translationVariables: {},
    initVariablesStatus: {},
  });
  const updateState = (updatedData) => {
    setState((prevState) => ({ ...prevState, ...updatedData }));
  };
  const [updatedValues, setUpdatedValues] = useState(null);
  useEffect(() => form.resetFields(), []);

  const onFinish = (values) => {
    updateState({ errorTranslation: null, generalError: null });
    onSubmit(convertTrueObjToObj(values), convertTrueObjToObj(updatedValues));
  };

  const onFinishFailed = (error) => {
    console.log('Form Error:', error);
    const { errorFields } = error;
    let generalError = null;
    let errorTranslation = null;

    for (const errorField of errorFields) {
      const fieldKeys = errorField.name[0].split('.');
      if (fieldKeys[0] === 'translations') {
        errorTranslation = errorTranslation || {};
        errorTranslation[fieldKeys[1]] = true;
      } else {
        generalError = generalError || {};
        generalError[errorField.name[0]] = true;
      }
    }
    updateState({ generalError, errorTranslation });
  };

  const getGeneralItems = (generalItems) => generalItems.map((formItem, i) => {
    const { render, name } = formItem;
    const option = { ...formItem };
    delete option.render;
    option.initialValue = _.get(initialValues, name);
    return (
      <Form.Item key={i} {...option}>
        {render()}
      </Form.Item>
    );
  });

  const getTranslationItems = (translationItems) => {
    const { errorTranslation } = state;
    const { supportLanguages } = language;
    const updateVariables = (name, languageId, content) => {
      const variables = (content.match(/{{([\w.\s]+)}}/igm) || []).map((v) => v.substr(2, v.length - 4));
      if (variables?.length) {
        const { translationVariables } = state;
        translationVariables[name] = translationVariables[name] || {};
        translationVariables[name][languageId] = variables;
        updateState({ translationVariables });
      }
    };
    return (
      <Tabs>
        {
          supportLanguages.map((supportLanguage) => {
            const { id: languageId, name: lang } = supportLanguage;
            return (
              <TabPane
                tab={(
                  <span style={{
                    color: errorTranslation?.[languageId] ? '#f5222d' : 'rgba(0, 0, 0, 0.65)',
                  }}
                  >
                    {lang}
                  </span>
                )}
                key={languageId}
                forceRender
              >
                {
                  translationItems.map((formItem, i) => {
                    const { render, name, extra } = formItem;
                    const option = { ...formItem };
                    option.name = `translations.${languageId}.${name}`;
                    option.getValueFromEvent = (event) => {
                      if (event?.target) {
                        const { target } = event;
                        const { value } = target;
                        updateVariables(name, languageId, value);
                        return value;
                      }
                      return event;
                    };
                    option.extra = state.translationVariables[name]?.[languageId]
                      ? `${t('variable')}: ${state.translationVariables[name][languageId]}\n${extra || ''}` : extra;
                    if (initialValues?.[name]?.[languageId]?.value) {
                      option.initialValue = initialValues[name][languageId].value;
                      if (!state.initVariablesStatus[name] || !state.initVariablesStatus[name][languageId]) {
                        updateVariables(name, languageId, initialValues[name][languageId].value);
                        const { initVariablesStatus } = state;
                        initVariablesStatus[name] = initVariablesStatus[name] || {};
                        initVariablesStatus[name][languageId] = true;
                        updateState({ initVariablesStatus });
                      }
                    }
                    delete option.render;
                    delete option.isTranslation;
                    return (
                      <Form.Item
                        key={i}
                        {...option}
                      >
                        {render()}
                      </Form.Item>
                    );
                  })
                }
              </TabPane>
            );
          })
        }
      </Tabs>
    );
  };

  const getItems = () => {
    const { errorTranslation, generalError } = state;
    const generalItems = [];
    const translationItems = [];
    for (const formItem of formItems(form)) {
      const { isTranslation } = formItem;
      if (isTranslation) {
        translationItems.push(formItem);
      } else {
        generalItems.push(formItem);
      }
    }
    const tabItems = [];
    if (generalItems.length) {
      const withError = generalError ? Object.keys(generalError).find((errorKey) => generalItems.find((generalItem) => generalItem.name === errorKey)) : null;
      tabItems.push({
        tab: (
          <span style={{
            color: withError ? '#f5222d' : 'rgba(0, 0, 0, 0.65)',
          }}
          >
            {t('general')}
          </span>
        ),
        key: 'general',
        items: getGeneralItems(generalItems),
      });
    }
    if (translationItems.length) {
      tabItems.push({
        tab: (
          <span style={{
            color: errorTranslation ? '#f5222d' : 'rgba(0, 0, 0, 0.65)',
          }}
          >
            {t('translation')}
          </span>
        ),
        key: 'translation',
        items: getTranslationItems(translationItems),
      });
    }
    if (subForms) {
      for (const subForm of subForms(form)) {
        const { label, index, items } = subForm;
        const subFormItems = items.map((item) => ({
          ...item,
          name: (index && `${index}.${item.name}`) || item.name,
        }));
        const withError = !!Object.keys(generalError || {}).find((errorKey) => errorKey.includes(index));
        tabItems.push({
          tab: (
            <span style={{
              color: withError ? '#f5222d' : 'rgba(0, 0, 0, 0.65)',
            }}
            >
              {label}
            </span>
          ),
          key: index,
          items: getGeneralItems(subFormItems),
        });
      }
    }
    if (tabItems.length === 1) {
      return tabItems[0].items;
    }
    return (
      <Tabs>
        {
          tabItems.map((tabItem) => {
            const { tab, key, items } = tabItem;
            return (
              <TabPane tab={tab} key={key} forceRender>
                {items}
              </TabPane>
            );
          })
        }
      </Tabs>
    );
  };

  return (
    <Form
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      form={form}
      onValuesChange={(changedValues) => {
        console.log('On Form Change:', changedValues);
        setUpdatedValues({ ...updatedValues, ...changedValues });
      }}
      {...formProps}
    >
      {getItems()}
      <div className={classes.footer}>
        {
          customFooter
            ? customFooter(() => form.resetFields(), updatedValues)
            : (
              <>
                <Button disabled={!updatedValues} loading={loading} type="primary" htmlType="submit">
                  {t('submit')}
                </Button>
                <Button
                  loading={loading}
                  onClick={() => {
                    form.resetFields();
                    setUpdatedValues(null);
                    if (onClear) {
                      onClear();
                    }
                  }}
                >
                  {t('reset')}
                </Button>
              </>
            )
        }
      </div>
    </Form>
  );
};

export default BaseForm;
