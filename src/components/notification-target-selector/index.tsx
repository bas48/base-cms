import { useEffect, useState } from 'react';
import { Input, Spin } from 'antd';
import DataSelector from '@components/data-selector';
import TypeInput from '@components/type-input';
import commonApi from '@utils/api/common.api';
import useCommon from '@utils/hooks/common';
import styles from './styles';

interface UserQueryProps {
  value?: number;
  onChange?: Function;
}

const UserQuerySelector = (props: UserQueryProps) => {
  const { value, onChange } = props;
  const { classes, language } = useCommon({
    withStyles: styles,
    observers: ['language'],
  });
  const { currentLanguage } = language;
  const [state, setState] = useState({
    queryId: null,
    queryVariables: [],
    queries: [],
  });
  const updateState = (updatedData) => {
    setState((prevState) => ({ ...prevState, ...updatedData }));
  };

  useEffect(() => {
    const getQuery = async () => {
      const queries = await commonApi.getSelectorItems('notification/user-query');
      updateState({ queries });
    };
    getQuery();
  }, []);

  const getQueryVariables = (variable) => {
    const queryVariables = [];
    for (const variableId of Object.keys(variable)) {
      queryVariables.push({
        id: variableId,
        type: variable[variableId],
      });
    }
    return queryVariables;
  };

  const valueHandler = (propsValue) => {
    if (propsValue && typeof propsValue === 'object') {
      const { queries } = state;
      const { queryId, variableValues } = propsValue;
      if (variableValues) {
        const selectedQuery = queries.find((query) => String(query.id) === queryId);
        const { variable } = selectedQuery;
        const queryVariables = getQueryVariables(variable);
        for (const variableKey of Object.keys(variableValues)) {
          const index = queryVariables.findIndex((queryVariable) => queryVariable.id === variableKey);
          if (index >= 0) {
            queryVariables[index].value = variableValues[variableKey];
          }
        }
        updateState({ queryVariables });
      }
      return updateState({ queryId });
    }
    return updateState({ queryId: propsValue });
  };

  useEffect(() => {
    if (state.queries?.length) {
      valueHandler(value);
    }
  }, [state.queries]);

  const onVariablesChange = (id, variableValue) => {
    const { queryId, queryVariables } = state;
    const index = queryVariables.findIndex((queryVariable) => queryVariable.id === id);
    queryVariables[index].value = variableValue;
    updateState({ queryVariables });
    let allFilled = true;
    const variableValues = {};
    for (const queryVariable of queryVariables) {
      if (!queryVariable.value) {
        allFilled = false;
      }
      variableValues[queryVariable.id] = queryVariable.value;
    }
    if (allFilled) {
      onChange({
        queryId,
        variableValues,
      });
    } else {
      onChange(null);
    }
  };

  const { queries, queryVariables, queryId } = state;
  return (
    queries?.length
      ? (
        <>
          <DataSelector
            allowClear
            value={queryId}
            onChange={(selected, selectedOption) => {
              if (selectedOption?.variable) {
                const { variable } = selectedOption;
                const variables = getQueryVariables(variable);
                if (variables?.length) {
                  updateState({ queryId: selected, queryVariables: variables });
                  onChange(null);
                } else {
                  updateState({ queryId: selected, queryVariables: [] });
                  onChange(selected);
                }
              } else {
                updateState({ queryId: selected, queryVariables: [] });
                onChange(selected);
              }
            }}
            dataSource={queries}
            defineValue={(option) => option.id}
            defineLabel={(option) => option?.title?.[currentLanguage]?.value || option.id}
          />
          {
            queryVariables.length
              ? queryVariables.map((queryVariable) => {
                const { id, type } = queryVariable;
                return (
                  <div className={classes.variableItem} key={id}>
                    <span className="label">{id}</span>
                    <div style={{ flex: 1 }} />
                    <TypeInput type={type} value={queryVariable.value} onChange={(variableValue) => onVariablesChange(id, variableValue)} />
                  </div>
                );
              }) : null
          }
        </>
      ) : <Spin />
  );
};

interface PropsType {
  value?: any;
  onChange?: Function;
}

const NotificationTargetSelector = (props: PropsType) => {
  const { onChange, value } = props;
  const { t, language } = useCommon({
    withStyles: styles,
    withTranslation: null,
    observers: ['language'],
  });
  const { currentLanguage } = language;
  const [state, setState] = useState(() => {
    if (value) {
      const { type, reference } = value;
      return {
        selectedType: type,
        extraValue: reference,
      };
    }
    return {
      selectedType: null,
      extraValue: undefined,
    };
  });
  const updateState = (updatedData) => {
    setState((prevState) => ({ ...prevState, ...updatedData }));
  };

  const updateExtraValue = (extraValue) => {
    const { selectedType } = state;
    updateState({ extraValue });
    if (extraValue) {
      onChange({
        type: selectedType,
        reference: extraValue,
      });
    } else {
      onChange(null);
    }
  };

  const getExtraContent = (selectedType) => {
    const { extraValue } = state;
    switch (selectedType) {
      case 'user':
        return (
          <UserQuerySelector
            value={extraValue}
            onChange={(newExtra) => updateExtraValue(newExtra)}
          />
        );
      case 'fcm_key':
        return (
          <Input
            value={extraValue}
            onChange={(e) => updateExtraValue(e.target.value)}
          />
        );
      case 'role':
        return (
          <DataSelector
            mode="multiple"
            dataPath="/v1/role"
            defineValue={(option) => option.id}
            defineLabel={(option) => option?.title?.[currentLanguage]?.value || option.id}
            value={extraValue}
            onChange={(newExtra) => updateExtraValue(newExtra)}
          />
        );
      default:
        return null;
    }
  };

  const { selectedType } = state;
  return (
    <>
      <DataSelector
        allowClear
        value={selectedType}
        dataPath="/v1/notification/target-types"
        defineValue={(option) => option}
        defineLabel={(option) => t(`notification_target_type_${option}`)}
        onChange={(type) => {
          updateState({ selectedType: type, extraValue: undefined });
          if (!getExtraContent(type)) {
            onChange({
              type,
            });
          } else {
            onChange(null);
          }
        }}
      />
      {
        getExtraContent(selectedType)
      }
    </>
  );
};

export default NotificationTargetSelector;
