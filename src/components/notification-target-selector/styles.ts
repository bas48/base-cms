const styles = {
  variableItem: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignContent: 'center',
    '& .label': {
      minWidth: 100,
    },
  },
};

export default styles;
