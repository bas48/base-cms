import { useState, useEffect } from 'react';
import { TreeSelect } from 'antd';
import permissionApi from '@utils/api/permission.api';
import useCommon from '@utils/hooks/common';

const { TreeNode } = TreeSelect;
interface PropsType {
  value?: any;
  onChange?: Function;
}

const PermissionGroupSelector = (props: PropsType) => {
  const { onChange, value } = props;
  const { language } = useCommon({
    observers: ['language'],
  });
  const { currentLanguage } = language;
  const [state, setState] = useState({
    permissions: [],
    localValue: null,
  });
  const updateState = (updatedData) => {
    setState((prevState) => ({ ...prevState, ...updatedData }));
  };

  useEffect(() => {
    const getPermissions = async () => {
      const permissions = await permissionApi.getPermissions();
      if (permissions) {
        updateState({
          permissions,
        });
      }
    };
    getPermissions();
  }, []);

  useEffect(() => {
    updateState({ localValue: value });
  }, [value]);

  const dataConvertor = (oriValue) => {
    if (oriValue) {
      const newValue = [];
      for (const key of Object.keys(oriValue)) {
        const exist = state.permissions.find((permission) => permission.id === key);
        if (exist) {
          newValue.push(`${key}:${oriValue[key]}`);
        }
      }
      return newValue;
    }
    return oriValue;
  };

  const { permissions, localValue } = state;
  return (
    <TreeSelect
      allowClear
      treeCheckable
      value={dataConvertor(localValue)}
      onChange={(newValue) => {
        let objValue = {};
        for (let i = 0; i < newValue.length; i += 1) {
          const valueKey = newValue[i].split(':');
          const permissionId = valueKey[0];
          const right = valueKey[1];
          objValue[permissionId] = right;
        }
        if (!Object.keys(objValue).length) {
          objValue = null;
        }
        if (onChange) {
          onChange(objValue);
        }
        updateState({ localValue: objValue });
      }}
    >
      {
        permissions.map((permission) => {
          const { id, title } = permission;
          const label = title?.[currentLanguage]?.value || id;
          return (
            <TreeNode selectable={false} value={id} title={label} key={id}>
              <TreeNode value={`${id}:read`} title={`${label}:read`} key={`${id}:read`} />
              <TreeNode value={`${id}:write`} title={`${label}:write`} key={`${id}:write`} />
            </TreeNode>
          );
        })
      }
    </TreeSelect>
  );
};

export default PermissionGroupSelector;
