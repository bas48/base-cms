const styles = {
  editableTable: {
    width: '100%',
  },
  addButton: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    '&:hover': {
      border: '2px #1890ff dotted',
      color: '#1890ff',
    },
  },
};

export default styles;
