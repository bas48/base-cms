/* eslint-disable no-script-url */
// eslint-disable-next-line no-use-before-define
import React, { useState, useEffect } from 'react';
import { Table, Input, InputNumber, Popconfirm, Form, Button } from 'antd';
import { Rule } from 'antd/lib/form';
import { PlusOutlined } from '@ant-design/icons';
import useCommon from '@utils/hooks/common';
import styles from './styles';

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> {
  editing: boolean;
  dataIndex: string;
  title: string;
  inputType: 'number' | 'text';
  record: any;
  index: number;
  editor?: Function;
  rules?: Rule[];
  children: React.ReactNode;
}

const EditableCell: React.FC<EditableCellProps> = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  editor,
  rules,
  ...restProps
}: EditableCellProps) => {
  const getInputNode = (type) => {
    switch (type) {
      case 'number':
        return <InputNumber />;
      default:
        return <Input />;
    }
  };
  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          rules={rules}
        >
          {editor ? editor(record) : getInputNode(inputType)}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

interface PropsType {
  columns: any[];
  dataSource?: any[];
  defaultData?: any;
  onChange?: Function;
}

const EditableTable = (props: PropsType) => {
  const { columns, dataSource, defaultData, onChange } = props;
  const { t, classes } = useCommon({
    withStyles: styles,
    withTranslation: ['editable-table'],
  });
  const [form] = Form.useForm();
  const [data, setData] = useState((dataSource?.map((record, i) => ({ ...record, key: i }))) || []);
  const [editingKey, setEditingKey] = useState(null);
  const [newKey, setNewKey] = useState(null);
  const [count, setCount] = useState((dataSource?.length) || 0);

  const isEditing = (record) => record.key === editingKey;

  const onDelete = (key) => setData(data.filter((item) => item.key !== key));

  const onCancel = () => {
    if (editingKey === newKey) {
      onDelete(newKey);
      setNewKey(null);
    }
    setEditingKey(null);
    form.resetFields();
  };

  const onEdit = (record) => {
    form.setFieldsValue(record);
    setEditingKey(record.key);
  };

  const onAdd = () => {
    form.resetFields();
    setData([...data, { ...defaultData, key: count }]);
    setCount(count + 1);
    setNewKey(count);
    setEditingKey(count);
  };

  const save = async (key: React.Key) => {
    try {
      const row = (await form.validateFields()) as any;
      const newData = [...data];
      const index = newData.findIndex((item) => key === item.key);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        setData(newData);
        setEditingKey('');
      } else {
        newData.push(row);
        setData(newData);
        setEditingKey('');
      }
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const mergedColumns = [...columns, {
    title: 'operation',
    dataIndex: 'operation',
    render: (_: any, record: any) => (isEditing(record) ? (
      <span>
        <Button type="link" href="javascript:;" onClick={() => save(record.key)}>
          {t('save')}
        </Button>
        <Popconfirm title={t('confirm_msg')} onConfirm={onCancel}>
          <Button type="link">{t('cancel')}</Button>
        </Popconfirm>
      </span>
    ) : (
      <span>
        <Button type="link" disabled={editingKey} onClick={() => onEdit(record)}>
          {t('edit')}
        </Button>
        <Popconfirm placement="topLeft" title={t('confirm_msg')} onConfirm={() => onDelete(record.key)}>
          <Button type="link">{t('delete')}</Button>
        </Popconfirm>
      </span>
    )),
  }].map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record: any) => ({
        record,
        inputType: col.inputType || 'text',
        editor: col.editor,
        rules: col.rules,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });

  const [initRun, setInitRun] = useState(true);
  useEffect(() => {
    if (!initRun && onChange) {
      onChange(data);
    } else {
      setInitRun(false);
    }
  }, [data]);

  return (
    <Form form={form} component={false}>
      <Table
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        bordered
        dataSource={data}
        columns={mergedColumns}
        rowClassName="editable-row"
        pagination={{
          onChange: onCancel,
        }}
        footer={() => (
          <Button
            disabled={editingKey}
            className={classes.addButton}
            onClick={() => onAdd()}
          >
            <PlusOutlined />
          </Button>
        )}
      />
    </Form>
  );
};

export default EditableTable;
