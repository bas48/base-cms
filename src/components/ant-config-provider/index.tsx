/* eslint-disable camelcase */
import { ConfigProvider } from 'antd';
import en_US from 'antd/lib/locale-provider/en_US';
import zh_TW from 'antd/lib/locale-provider/zh_TW';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import ko_KR from 'antd/lib/locale-provider/ko_KR';
import ja_JP from 'antd/lib/locale-provider/ja_JP';
import useCommon from '@utils/hooks/common';

// eslint-disable-next-line react/prop-types
const AntConfigProvider = ({ children }) => {
  const { currentLanguage } = useCommon({ observers: [({ language }) => () => ({
    currentLanguage: language.currentLanguage,
  })] });
  const getLocale = () => {
    switch (currentLanguage) {
      case 'zh-HK':
        return zh_TW;
      case 'zh-TW':
        return zh_TW;
      case 'zh-CN':
        return zh_CN;
      case 'ja-JP':
        return ja_JP;
      case 'ko-KR':
        return ko_KR;
      default:
        return en_US;
    }
  };
  return (
    <ConfigProvider locale={getLocale()}>
      {children}
    </ConfigProvider>
  );
};

export default AntConfigProvider;
