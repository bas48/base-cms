const styles = {
  listPage: {
  },
  header: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    background: 'white',
    padding: 0,
  },
  searchBar: {
    flex: 1,
    marginRight: 10,
  },
};

export default styles;
