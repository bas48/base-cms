/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable no-case-declarations */
import { useState, useEffect, useRef } from 'react';
import { useDebounce } from 'ahooks';
import { Button, Card, Input, Layout, Modal, Popover, Table, Row, Col, Popconfirm } from 'antd';
import { FilterOutlined, PlusOutlined, ExportOutlined } from '@ant-design/icons';
import commonApi from '@utils/api/common.api';
import useCommon from '@utils/hooks/common';
import useRequest from '@utils/hooks/request';
import styles from './styles';

const { Header } = Layout;
const { Search } = Input;

interface SortParam {
  orderBy: string,
  direction: string,
}

interface PropsType {
  // eslint-disable-next-line no-unused-vars
  columns: (...props: any) => any[];
  apiPath?: string;
  // eslint-disable-next-line no-unused-vars
  getData?: (page: number, sort: SortParam, limit: number, search?: string, filter?: object, exportData?: boolean) => Promise<{
    count: number;
    rows: any[];
  }> | {
    count: number;
    rows: any[];
  };
  searchInfo?: string;
  title: string | any;
  showSizeChanger?: boolean;
  children?: any;
  filter?: Function;
  createBoard?: any;
  createBoardProps?: any;
  onCreateBoardClose?: Function;
  updateBoard?: any;
  updateBoardProps?: any;
  onUpdateBoardClose?: Function;
  defaultState?: any;
  showPagination?: boolean;
  addBeforeHeader?: Function;
  customHeader?: Function;
  addAfterHeader?: Function;
  exportEnable?: boolean;
  multiSortEnable?: boolean;
  className?: any;
}
const usePrevious = (value) => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
};

const ListPage = (props: PropsType) => {
  const { t, classes } = useCommon({
    withStyles: styles,
    withTranslation: ['list-page'],
  });
  const { columns, searchInfo, title, showSizeChanger = true, children, customHeader, addBeforeHeader, addAfterHeader, multiSortEnable = true, className } = props;
  const { filter, createBoard, updateBoard, createBoardProps, updateBoardProps, onCreateBoardClose, onUpdateBoardClose, defaultState = {}, showPagination = true, exportEnable } = props;
  const [state, setState] = useState({
    page: 1,
    sort: [{ orderBy: 'createdAt', direction: 'ASC' }],
    pageSize: 10,
    total: 0,
    search: '',
    dataSource: [],
    filterData: {},
    showFilter: false,
    showCreateBoard: false,
    showUpdateBoard: false,
    selectedRecord: null,
    ...defaultState,
  });
  const searchValue = useDebounce(state.search, { wait: 500 });
  const prevSearch = usePrevious(searchValue);

  const updateState = (updatedData) => {
    setState((prevState) => ({ ...prevState, ...updatedData }));
  };

  const { loading: getLoading, run: getDataSource } = useRequest(async () => {
    const { sort, pageSize, filterData } = state;
    const { apiPath, getData } = props;
    let { page } = state;
    if (searchValue) {
      page = prevSearch === '' ? 1 : page;
    }
    let response: any;
    if (getData) {
      response = await getData(page, sort, pageSize, searchValue, filterData);
    } else {
      response = await commonApi.getList(apiPath, page, sort, pageSize, searchValue, filterData);
    }
    updateState({
      page,
      total: response.count,
      dataSource: response.rows.map((record, i) => {
        if (!record.key) {
          return { ...record, key: i };
        }
        return record;
      }),
    });
  }, { showError: true });

  const { loading: exportLoading, run: exportData } = useRequest(async () => {
    const { page, sort, pageSize, filterData } = state;
    const { apiPath, getData } = props;
    let response: any;
    if (getData) {
      response = await getData(page, sort, pageSize, searchValue, filterData, true);
    } else {
      response = await commonApi.getList(apiPath, page, sort, pageSize, searchValue, filterData, true);
    }
    const { type, file, fileName, target } = response;
    switch (type) {
      case 'download':
        const link = document.createElement('a');
        link.href = `data:text/csv;charset=utf-8,${encodeURIComponent(file)}`;
        link.download = `${fileName || 'export'}.csv`;
        link.click();
        break;
      case 'email':
        Modal.info({
          title: t('export_title'),
          content: (
            <div>
              {t('export_info', { target })}
            </div>
          ),
          onOk() { },
        });
        break;
      default:
        break;
    }
  }, { showError: true });

  useEffect(() => {
    getDataSource();
  }, [state.page, state.pageSize, state.sort, state.filterData]);

  useEffect(() => {
    if (state.page === 1) {
      getDataSource();
    } else {
      updateState({ page: 1 });
    }
  }, [searchValue]);

  useEffect(() => {
  }, [state.dataSource]);

  const onCreateClose = (refreshData = false) => {
    if (onCreateBoardClose) {
      onCreateBoardClose();
    }
    updateState({ showCreateBoard: false });
    if (refreshData) {
      getDataSource();
    }
  };

  const onUpdateClose = (refreshData = false) => {
    if (onUpdateBoardClose) {
      onUpdateBoardClose();
    }
    updateState({ showUpdateBoard: false });
    if (refreshData) {
      getDataSource();
    }
  };

  const { loading: createLoading, run: onCreate } = useRequest(async (data, callBack) => {
    console.log('On Create:', data);
    const { apiPath } = props;
    const record = await commonApi.createRecord(apiPath, data);
    console.log('record:', record);
    if (callBack) {
      await callBack(record);
    }
    onCreateClose(true);
  }, { showError: true, successMessage: t('translation:success') });

  const { loading: updateLoading, run: onUpdate } = useRequest(async (recordId, data, callBack) => {
    console.log('On Update:', recordId, data);
    const { apiPath } = props;
    const record = await commonApi.updateRecord(apiPath, recordId, data);
    if (callBack) {
      await callBack(record);
    }
    onUpdateClose(true);
  }, { showError: true, successMessage: t('translation:success') });

  const { loading: deleteLoading, run: onDelete } = useRequest(async (recordId) => {
    console.log('On Delete:', recordId);
    const { apiPath } = props;
    const { dataSource, page } = state;
    const shouldRefresh = dataSource.length === 1;
    await commonApi.deleteRecord(apiPath, recordId);
    if (shouldRefresh && page > 1) {
      updateState({ page: page - 1 });
    } else {
      getDataSource();
    }
  }, { showError: true });

  const switchCreateBoard = () => updateState({ showCreateBoard: true });
  const onSearch = (searchText: string) => updateState({ search: searchText });
  const submitFilterValue = (filterData) => updateState({ filterData, showFilter: false, page: 1 });
  const resetFilter = () => updateState({ filterData: {}, showFilter: false, page: 1 });

  const { page, pageSize, total, search, dataSource, showFilter, showCreateBoard, showUpdateBoard, selectedRecord } = state;
  return (
    <Card title={title} className={className}>
      {addBeforeHeader && addBeforeHeader()}
      {
        (customHeader && customHeader(switchCreateBoard, onSearch, submitFilterValue, resetFilter))
        || (
          (searchInfo || filter || createBoard || exportEnable)
          && (
            <Header className={classes.header}>
              {searchInfo && (
                <Search
                  className={classes.searchBar}
                  enterButton
                  placeholder={t('search_bar_placeholder', { field: searchInfo })}
                  value={search}
                  onChange={(e) => onSearch(e.target.value)}
                />
              )}
              <Row gutter={10}>
                {filter && (
                  <Col>
                    <Popover
                      placement="bottomLeft"
                      visible={showFilter}
                      onVisibleChange={(value) => updateState({ showFilter: value })}
                      content={filter(
                        submitFilterValue,
                        resetFilter,
                        getLoading,
                      )}
                      title={t('filter')}
                      trigger="click"
                    >
                      <Button loading={getLoading}><FilterOutlined /></Button>
                    </Popover>
                  </Col>
                )}
                {createBoard && (
                  <Col>
                    <Button
                      onClick={switchCreateBoard}
                    >
                      <PlusOutlined />
                    </Button>
                  </Col>
                )}
                {
                  exportEnable && (
                    <Col>
                      <Popconfirm
                        title={t('confirm_to_export')}
                        onConfirm={exportData}
                        okText={t('translation:yes')}
                        cancelText={t('translation:no')}
                      >
                        <Button
                          loading={exportLoading}
                        >
                          <ExportOutlined />
                        </Button>
                      </Popconfirm>
                    </Col>
                  )
                }
              </Row>
            </Header>
          ))
      }
      {addAfterHeader && addAfterHeader()}
      <Table
        columns={columns(
          (record) => updateState({ showUpdateBoard: true, selectedRecord: record }),
          (recordId) => onDelete(recordId),
          deleteLoading,
          () => getDataSource(state),
        ).map((col) => {
          const { sorter } = col;
          if (sorter && multiSortEnable) {
            Object.assign(col, {
              sorter: { multiple: 1 },
            });
          }
          return col;
        })}
        dataSource={dataSource}
        loading={getLoading}
        pagination={(showPagination && {
          showSizeChanger,
          current: page,
          pageSize,
          total,
        }) || false}
        onChange={(pagination, filters, sorter: any) => {
          const parseDirection = (fullString: string) => (fullString === 'ascend' ? 'ASC' : 'DESC');
          let newSortList = [];
          if (Array.isArray(sorter)) {
            newSortList = sorter.map((col) => ({ orderBy: col.field, direction: parseDirection(col.order) }));
          } else if (Object.keys(sorter).length !== 0) {
            newSortList = [{ orderBy: sorter.field, direction: parseDirection(sorter.order) }];
          } else {
            newSortList = state.sort;
          }
          updateState({
            page: pagination.current,
            pageSize: pagination.pageSize,
            sort: newSortList,
          });
        }}
      />
      {createBoard && (
        <Modal
          bodyStyle={{ padding: 0 }}
          visible={showCreateBoard}
          footer={null}
          maskClosable={false}
          destroyOnClose
          onCancel={() => onCreateClose()}
          {...createBoardProps}
        >
          <Card title={t('create_board')}>
            {createBoard((data, callBack) => onCreate(data, callBack), createLoading)}
          </Card>
        </Modal>
      )}
      {updateBoard && (
        <Modal
          bodyStyle={{ padding: 0 }}
          visible={showUpdateBoard}
          footer={null}
          maskClosable={false}
          destroyOnClose
          onCancel={() => {
            onUpdateClose();
            updateState({ selectedRecord: null });
          }}
          {...updateBoardProps}
        >
          <Card title={t('update_board')}>
            {updateBoard(
              selectedRecord,
              (recordId, data, callBack) => onUpdate(recordId, data, callBack),
              updateLoading,
            )}
          </Card>
        </Modal>
      )}
      {children}
    </Card>
  );
};

export default ListPage;
