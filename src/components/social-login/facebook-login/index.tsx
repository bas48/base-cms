import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import useCommon from '@utils/hooks/common';
import Router from 'next/router';
import getConfig from 'next/config';
import { FacebookOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import classnames from 'classnames';
import styles from './styles';

const { REACT_FACEBOOK_APP_ID } = getConfig().publicRuntimeConfig;

interface PropsType {
  className?: any;
  [key: string]: any;
}

const SocialLogin = (props: PropsType) => {
  const { classes, user } = useCommon({
    withStyles: styles,
    observers: ['user'],
  });

  const { className } = props;

  const responseFacebook = async (response) => {
    const { accessToken } = response;
    await user.socialLogin(accessToken, 'facebook');
    Router.push('/');
  };

  return (
    <FacebookLogin
      className={className}
      appId={REACT_FACEBOOK_APP_ID}
      callback={(response) => responseFacebook(response)}
      size="small"
      icon="fa-facebook"
      render={(renderProps) => (
        <div className={classes.facebookLogin}>
          <Button
            className={classnames(className, classes.facebookButton)}
            onClick={renderProps.onClick}
            disabled={renderProps.disabled}
          >
            <FacebookOutlined />
          </Button>
          Facebook
        </div>
      )}
    />
  );
};

export default SocialLogin;
