const styles = {
  facebookLogin: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  facebookButton: {
    width: 70,
    height: 70,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    fontSize: 25,
    color: 'white',
    background: '#4267B2',
    boxShadow: '2px 2px 15px -10px rgba(0,0,0,0.75)',
  },
};

export default styles;
