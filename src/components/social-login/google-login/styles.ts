const styles = {
  googleLogin: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  googleButton: {
    width: 70,
    height: 70,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    fontSize: 25,
    color: 'white',
    background: '#4285F4',
    boxShadow: '2px 2px 15px -10px rgba(0,0,0,0.75)',
  },
};

export default styles;
