/* eslint-disable camelcase */
import GoogleLogin from 'react-google-login';
import useCommon from '@utils/hooks/common';
import Router from 'next/router';
import getConfig from 'next/config';
import { Button } from 'antd';
import { GoogleOutlined } from '@ant-design/icons';
import classnames from 'classnames';
import styles from './styles';

const { REACT_GOOGLE_CLIENT_ID } = getConfig().publicRuntimeConfig;

interface PropsType {
  className?: any;
  [key: string]: any;
}

const SocialLogin = (props: PropsType) => {
  const { classes, user } = useCommon({
    withStyles: styles,
    observers: ['user'],
  });

  const { className } = props;

  const responseGoogle = async (response) => {
    const authResponse = response.getAuthResponse();
    const { id_token } = authResponse;
    await user.socialLogin(id_token, 'google');
    Router.push('/');
  };

  return (
    <GoogleLogin
      className={className}
      clientId={REACT_GOOGLE_CLIENT_ID}
      onSuccess={(response) => responseGoogle(response)}
      onFailure={(response) => responseGoogle(response)}
      cookiePolicy="single_host_origin"
      scope="https://www.googleapis.com/auth/userinfo.profile"
      render={(renderProps) => (
        <div className={classes.googleLogin}>
          <Button
            className={classnames(className, classes.googleButton)}
            onClick={renderProps.onClick}
            disabled={renderProps.disabled}
          >
            <GoogleOutlined />
          </Button>
          Google
        </div>
      )}
    />
  );
};

export default SocialLogin;
