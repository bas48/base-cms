const styles = {
  uploadButton: {
    '.ant-upload.ant-upload-select-picture-card:hover &': {
      color: '#1890ff',
    },
    '& .remind': {
      maxWidth: 86,
      fontSize: 12,
    },
  },
};

export default styles;
