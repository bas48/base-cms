/* eslint-disable camelcase */
import { useEffect, useState } from 'react';
import { Upload, Modal, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import mime from 'mime-types';
import getConfig from 'next/config';
import { v4 as uuidv4 } from 'uuid';
import fileApi from '@utils/api/file.api';
import useCommon from '@utils/hooks/common';
import useRequest from '@utils/hooks/request';
import styles from './styles';

const { publicRuntimeConfig } = getConfig();
const server_url = publicRuntimeConfig.REACT_URL;

interface PropsType {
  limit?: number;
  sizeLimit?: number;
  accept?: string;
  remind?: string;
  value?: any;
  onChange?: Function;
}

const fileConvertor: any = (path) => {
  const pathText = path.split('/');
  const name = pathText[pathText.length - 1];
  const type = mime.lookup(path);
  const ext = mime.extension(type);
  const mainType = (type && type.split('/')[0]) || null;
  return {
    uid: uuidv4(),
    name,
    status: 'done',
    url: `${server_url}${path}`,
    path,
    type,
    mainType,
    ext,
  };
};

const FileUpload = (props: PropsType) => {
  const { limit, sizeLimit, accept, remind, value, onChange } = props;
  const { t, classes } = useCommon({
    withStyles: styles,
    withTranslation: ['file-upload'],
  });
  const [initRun, setInitRun] = useState(true);
  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewFile, setPreviewFile] = useState(null);
  const [fileList, setFileList] = useState(() => {
    if (value) {
      if (Array.isArray(value) && value.length) {
        return value.map((filePath) => fileConvertor(filePath));
      }
      return [fileConvertor(value)];
    }
    return [];
  });

  const getBase64 = (file) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

  const onPreview = async (file) => {
    const { mainType } = file;
    if (mainType === 'video' || mainType === 'image') {
      if (!file.url && !file.preview) {
        // eslint-disable-next-line
        file.preview = await getBase64(file.originFileObj);
      }
      setPreviewFile(file);
      setPreviewVisible(true);
    }
  };

  useEffect(() => {
    if (!initRun) {
      if (fileList?.length) {
        let newValue = [];
        if (limit === 1) {
          newValue = fileList[0].path;
        } else {
          const lastIndex = fileList.length;
          for (let i = 0; i < lastIndex; i += 1) {
            newValue.push(fileList[i].path);
          }
        }
        onChange(newValue);
      } else {
        onChange(null);
      }
    } else {
      setInitRun(false);
    }
  }, [fileList]);

  const uploadButton = (
    <div className={classes.uploadButton}>
      <PlusOutlined />
      <div className="text">{t('upload')}</div>
      {remind ? <div className="remind">{remind}</div> : null}
    </div>
  );

  const getPreviewContent = (file) => {
    const { mainType, type, url, preview } = file;
    switch (mainType) {
      case 'image':
        return (
          <img alt="preview" style={{ width: '100%' }} src={url || preview} />
        );
      case 'video':
        return (
          <video
            style={{ width: '100%' }}
            preload="auto"
            controls
          >
            <source src={url} type={type} />
            <track kind="captions" />
          </video>
        );
      default:
        return null;
    }
  };

  const { loading, run: upload } = useRequest(async (file, mainType) => {
    const path = await fileApi.uploadTemporaryFile(file, mainType);
    return path;
  }, { showError: true });

  const beforeUpload = (file) => {
    if (sizeLimit && (file.size / 1000) > sizeLimit * 1024) {
      message.error(t('error:file_size_limit_error', { size: sizeLimit }));
      return false;
    }
    return true;
  };

  return (
    <div>
      <Upload
        accept={accept}
        disabled={loading}
        customRequest={async (request: any) => {
          const { onError, onSuccess, file } = request;
          const { type } = file;
          try {
            const mainType = type.split('/')[0];
            const path = await upload(file, mainType);
            const convertedFile = fileConvertor(path);
            const newFileList = [...fileList, convertedFile];
            setFileList(newFileList);
            onSuccess({ path });
          } catch (error) {
            onError(error);
          }
        }}
        listType="picture-card"
        fileList={fileList}
        beforeUpload={beforeUpload}
        // iconRender={(file: any) => {
        //   const { ext } = file;
        //   return (
        //     <img
        //       alt="icon"
        //       src={`/file-format/${ext || 'file'}.png`}
        //     />
        //   );
        // }}
        onPreview={onPreview}
        onRemove={(removedFile) => {
          const removeIndex = fileList.findIndex((file) => file === removedFile);
          if (removeIndex >= 0) {
            const newFileList = [...fileList];
            newFileList.splice(removeIndex, 1);
            setFileList(newFileList);
          }
        }}
      >
        {limit && fileList.length >= limit ? null : uploadButton}
      </Upload>
      <Modal
        width={700}
        visible={previewVisible}
        footer={null}
        onCancel={() => {
          setPreviewVisible(false);
          setPreviewFile(null);
        }}
      >
        {previewFile && getPreviewContent(previewFile)}
      </Modal>
    </div>
  );
};

export default FileUpload;
