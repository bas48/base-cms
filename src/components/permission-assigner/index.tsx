import { useEffect, useState } from 'react';
import { Radio } from 'antd';
import permissionApi from '@utils/api/permission.api';
import BaseForm from '@components/base-form';
import useCommon from '@utils/hooks/common';
import styles from './styles';

interface PropsType {
  loading?: boolean;
  initialValues?: any;
  onSubmit: Function;
  supportBanned ?: boolean;
}

const PermissionAssigner = (props: PropsType) => {
  const { supportBanned } = props;
  const formProps = { ...props };
  delete formProps.supportBanned;
  const { t, language } = useCommon({
    withTranslation: ['permission-assigner'],
    withStyles: styles,
    observers: ['language'],
  });
  const { currentLanguage } = language;
  const [permissions, setPermissions] = useState([]);

  useEffect(() => {
    const getPermission = async () => {
      const record = await permissionApi.getPermissions();
      setPermissions(record);
    };
    if (!permissions.length) {
      getPermission();
    }
  }, [permissions]);

  return (
    permissions?.length
      ? (
        <BaseForm
          formProps={{
            labelCol: { span: 8 },
            wrapperCol: { span: 16 },
          }}
          formItems={() => permissions.map((permission) => {
            const { id, title } = permission;
            const label = title?.[currentLanguage]?.value || id;
            return {
              name: permission.id,
              label,
              render: () => (
                <Radio.Group>
                  <Radio.Button value="read">{t('read')}</Radio.Button>
                  <Radio.Button value="write">{t('write')}</Radio.Button>
                  <Radio.Button value={undefined}>{t('none')}</Radio.Button>
                  {
                  supportBanned
                    ? <Radio.Button value="banned">{t('banned')}</Radio.Button> : null
                }
                </Radio.Group>
              ),
            };
          })}
          {...formProps}
        />
      ) : null
  );
};

export default PermissionAssigner;
