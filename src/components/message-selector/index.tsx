import DataSelector from '@components/data-selector';
import useCommon from '@utils/hooks/common';

interface PropsType {
  value?: number;
  onChange?: Function;
  onHandlerParams?: Function;
}

const MessageSelector = (props: PropsType) => {
  const { value, onChange, onHandlerParams } = props;
  const { language } = useCommon({
    observers: ['language'],
  });
  const { currentLanguage } = language;
  const getHandlerParams = (option) => {
    const handlerParams = [];
    const { handlers } = option;
    if (handlers) {
      for (const handlerId of Object.keys(handlers)) {
        const { params } = handlers[handlerId];
        if (params) {
          for (const paramId of Object.keys(params)) {
            const { title, type, options } = params[paramId];
            handlerParams.push({
              handler: {
                id: handlerId,
                title: handlers[handlerId].title,
              },
              index: paramId,
              label: title,
              dataType: type,
              options,
            });
          }
        }
      }
    }
    return handlerParams;
  };

  return (
    <DataSelector
      value={value}
      dataPath="/v1/message"
      defineValue={(option) => option.id}
      defineLabel={(option) => (option?.title?.[currentLanguage]?.value)}
      onDefault={(selectedValue, option) => {
        const handlerParams = getHandlerParams(option);
        if (onHandlerParams) {
          onHandlerParams(handlerParams);
        }
      }}
      onChange={(selectedValue, option) => {
        const handlerParams = getHandlerParams(option);
        if (onChange) {
          onChange(selectedValue, option);
        }
        if (onHandlerParams) {
          onHandlerParams(handlerParams);
        }
      }}
    />
  );
};

export default MessageSelector;
