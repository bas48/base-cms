import useCommon from '@utils/hooks/common';
import { Input, InputNumber, DatePicker, Select } from 'antd';
import moment from 'moment';

const { Option } = Select;

interface PropsType {
  type: string;
  value?: any;
  onChange?: Function;
  itemProps?: any;
  options?: any;
}

const TypeInput = (props: PropsType) => {
  const { type, value, onChange, itemProps, options } = props;
  const { t } = useCommon({
    withTranslation: null,
  });

  if (options?.length) {
    return (
      <Select
        {...itemProps}
        style={{ minWidth: 120, width: 'fit-content' }}
        value={value}
        onChange={(...selectProps) => onChange(...selectProps)}
      >
        {
          options.map((option) => {
            const { label } = option;
            return (
              <Option key={option.value} value={option.value}>{label}</Option>
            );
          })
        }
      </Select>
    );
  }
  switch (type) {
    case 'number':
      return (
        <InputNumber
          {...itemProps}
          value={value}
          onChange={(...selectProps) => onChange(...selectProps)}
        />
      );
    case 'date':
      return (
        <DatePicker
          showTime
          {...itemProps}
          value={value ? moment(value) : undefined}
          onChange={(...selectProps) => onChange(...selectProps)}
        />
      );
    case 'boolean':
      return (
        <Select
          {...itemProps}
          style={{ minWidth: 120, width: 'fit-content' }}
          value={value}
          onChange={(...selectProps) => onChange(...selectProps)}
        >
          <Option value="true">{t('translation:yes')}</Option>
          <Option value="false">{t('translation:no')}</Option>
        </Select>
      );
    default:
      return (
        <Input
          style={{ width: 184 }}
          value={value}
          {...itemProps}
          onChange={(e) => onChange(e.target.value)}
        />
      );
  }
};

export default TypeInput;
