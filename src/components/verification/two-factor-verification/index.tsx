import { useState, useEffect } from 'react';
import { Result } from 'antd';
import CodeInput from '@components/verification/code-input';
import verificationApi from '@utils/api/verification.api';
import useCommon from '@utils/hooks/common';
import useRequest from '@utils/hooks/request';

const codeLength = 6;

interface PropsType {
  onToken?: Function;
  verifyRequired?: boolean;
}

const TwoFAVerification = (props: PropsType) => {
  const { onToken, verifyRequired } = props;
  const { t } = useCommon({
    withTranslation: ['verification'],
  });
  const [state, setState] = useState({
    status: 'input',
    twoFACode: null,
    loading: false,
    showTimer: false,
    token: null,
  });
  const updateState = (updatedData) => {
    setState((prevState) => ({ ...prevState, ...updatedData }));
  };

  useEffect(() => {
    setTimeout(() => {
      if (state.token && onToken) {
        onToken(state.token);
      }
    }, 1500);
  }, [state.token]);

  const { run: createTwoFARecord, loading } = useRequest(async (code) => {
    const token = await verificationApi.createTwoFARecord(code);
    updateState({ token, status: 'success' });
  }, { showError: true });

  const getContent = () => {
    const { status } = state;
    switch (status) {
      case 'input':
        return (
          <CodeInput
            disabled={loading}
            length={codeLength}
            onComplete={(code) => {
              if (verifyRequired) {
                createTwoFARecord(code);
              } else if (code && onToken) {
                onToken(code);
              }
            }}
          />
        );
      default:
        return (
          <Result
            status="success"
            title={t('successful_verify')}
          />
        );
    }
  };

  const content = getContent();
  return content;
};

export default TwoFAVerification;
