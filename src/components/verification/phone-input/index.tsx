import { useEffect, useState } from 'react';
import { Input, Select } from 'antd';
import parsePhoneNumber from 'libphonenumber-js';
import metadata from 'libphonenumber-js/metadata.min.json';
import useCommon from '@utils/hooks/common';
import styles from './styles';

const { Option } = Select;

interface PropsType {
  defaultValue?: string,
  disabled?: boolean,
  addonAfter?: any,
  onChange?: Function,
  onKeyDown?: Function,
}

const PhoneInput = (props: PropsType) => {
  const { t } = useCommon({
    withTranslation: ['phone-input'],
    withStyles: styles,
  });
  const { defaultValue, disabled, addonAfter, onChange, onKeyDown } = props;
  let defaultCode;
  let defaultNumber;
  if (defaultValue) {
    const parse = parsePhoneNumber(defaultValue);
    defaultCode = `+${parse.countryCallingCode}` || t('invalid_country_code');
    defaultNumber = parse.nationalNumber || t('invalid_phone');
  }
  const countryCodeOptions = Object.keys(metadata.country_calling_codes).map((code) => ({
    label: `+${code}`,
    value: `+${code}`,
  }));
  const [countryCode, setCountryCode] = useState(defaultCode);
  const [number, setNumber] = useState(defaultNumber);
  const [initRun, setInitRun] = useState(true);

  useEffect(() => {
    if (!initRun) {
      const parse = parsePhoneNumber(`${countryCode}${number}`);
      if (parse?.countryCallingCode && parse.nationalNumber) {
        onChange(`${countryCode}${number}`);
      } else {
        onChange(null);
      }
    } else {
      setInitRun(false);
    }
  }, [countryCode, number]);

  return (
    <Input
      value={number}
      placeholder={t('phone_number')}
      disabled={disabled}
      onKeyDown={(e) => (onKeyDown ? onKeyDown(e.keyCode) : null)}
      addonBefore={(
        <Select
          value={countryCode}
          showSearch
          disabled={disabled}
          onChange={(code) => setCountryCode(code)}
          style={{ width: 80 }}
          placeholder={(t('phone_code'))}
        >
          {
            countryCodeOptions.map((code) => (
              <Option key={code.value} value={code.value}>{code.label}</Option>
            ))
          }
        </Select>
      )}
      onChange={(e) => setNumber(e.target.value)}
      addonAfter={addonAfter}
    />
  );
};

export default PhoneInput;
