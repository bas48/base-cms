import { useEffect, useState } from 'react';
import { Progress } from 'antd';

interface PropsType {
  count: number;
  onFinish?: Function,
}

const CountDown = (props: PropsType) => {
  const { count, onFinish } = props;
  const [currentCount, setCurrentCount] = useState(count);
  const [currentTimer, setCurrentTimer] = useState(null);

  const stopTimer = () => {
    clearInterval(currentTimer);
    setCurrentTimer(null);
  };

  const startTimer = () => {
    if (!currentTimer) {
      const timerId = setInterval(() => {
        const newCount = currentCount - 0.01;
        setCurrentCount(newCount);
        if (newCount <= 0) {
          stopTimer();
          if (onFinish) {
            onFinish();
          }
        }
      }, 10);
      setCurrentTimer(timerId);
    }
  };

  useEffect(() => {
    startTimer();
    return () => {
      if (currentTimer) {
        stopTimer();
      }
    };
  });

  const getStatusColor = (present) => {
    if (present <= 100 && present > 75) {
      return '#2ECC71';
    } if (present <= 75 && present > 50) {
      return '#F4D03F';
    } if (present <= 50 && present > 25) {
      return '#E59866';
    }
    return '#E74C3C';
  };

  const present = (currentCount / count) * 100;
  return (
    <Progress
      type="circle"
      percent={present}
      strokeColor={getStatusColor(present)}
      format={() => (Math.ceil(currentCount))}
    />
  );
};

export default CountDown;
