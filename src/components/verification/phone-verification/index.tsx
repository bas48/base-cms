import { useState } from 'react';
import useCommon from '@utils/hooks/common';
import useRequest from '@utils/hooks/request';
import { Form, Button, Input } from 'antd';
import { useStopwatch } from 'react-timer-hook';
import verificationApi from '@utils/api/verification.api';
import PhoneInput from '@components/verification/phone-input';
import styles from './styles';

interface PropsType {
  form: any;
  onToken: Function;
  isGuest?: boolean;
}

const PhoneVerification = (props: PropsType) => {
  const { form, onToken, isGuest = true } = props;
  const { t, classes } = useCommon({
    withStyles: styles,
    withTranslation: ['verification'],
  });
  const timer = useStopwatch({ autoStart: false });
  const [sent, setSent] = useState(false);
  const [code, setCode] = useState(null);
  const [token, setToken] = useState(null);
  const resendLimit = 60;

  const { run: sendCode, loading: sendLoading } = useRequest(async (phone) => {
    const response = await verificationApi.sendCode('phone', null, phone, isGuest);
    console.log('Code:', response);
    if (!sent) {
      setSent(true);
    }
    timer.start();
  }, { showError: true });

  const { run: verifyCode, loading: verifyLoading } = useRequest(async (phone) => {
    const response = await verificationApi.verifyCode('phone', code, null, phone);
    onToken(response);
    setToken(response);
  }, { showError: true, successMessage: t('translation:success') });

  if (timer.minutes === 1) {
    timer.reset();
    timer.pause();
    timer.isRunning = false;
  }

  const reset = () => {
    setToken(null);
    setCode(null);
    setSent(false);
    onToken(null);
    timer.reset();
    form.setFieldsValue({
      verifyCode: null,
    });
  };

  return (
    <>
      <Form.Item
        name="phone"
        rules={[
          { whitespace: true, message: t('error:please_input', { field: t('phone_number') }) },
          { required: true, message: t('error:please_input', { field: t('phone_number') }) },
        ]}
        valuePropName="defaultValue"
      >
        <PhoneInput
          onChange={() => {
            if (token) {
              reset();
            }
          }}
        />
      </Form.Item>
      {
        token
          ? null
          : (
            <div className={classes.verifyInput}>
              <Form.Item
                className="input"
                name="phoneVerifyCode"
                rules={[
                  { required: true, message: t('error:please_input', { field: t('phone_verification_code') }) },
                  {
                    validator(rule, value) {
                      if (!value || token) {
                        return Promise.resolve();
                      }
                      return Promise.reject(t('phone_not_verify_error'));
                    },
                  },
                ]}
              >
                <Input
                  placeholder={t('phone_verification_code')}
                  disabled={!sent}
                  onChange={(e) => setCode(e.target.value)}
                />
              </Form.Item>
              <Button
                className="send-button"
                type="primary"
                loading={sendLoading || verifyLoading}
                disabled={timer.isRunning && !code}
                onClick={async () => {
                  const result = await form.validateFields(['phone']);
                  const { phone } = result;
                  if (code) {
                    verifyCode(phone);
                  } else {
                    sendCode(phone);
                  }
                }}
              >
                {
                  (code && t('confirm'))
                  || (timer.isRunning && t('timer_count', { count: resendLimit - timer.seconds }))
                  || (sent && t('resend_verification_code'))
                  || t('send_verification_code')
                }
              </Button>
            </div>
          )
      }
    </>
  );
};

export default PhoneVerification;
