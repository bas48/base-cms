const styles = {
  board: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  codeInput: {
    width: 200,
    marginBottom: 10,
  },
  verifyBtn: {
    color: '#1890ff',
    cursor: 'pointer',
    '&:hover': {
      color: '#99ceff',
    },
  },
  disabledBtn: {
    cursor: 'no-drop',
  },
};

export default styles;
