const desktop = () => ({
  verifyInput: {
    display: 'flex',
    '& .input': {
      flex: 1,
    },
    '& .send-button': {
      marginLeft: 10,
    },
  },
});

const styles = (theme) => {
  const { commonClass, classesMerge } = theme;
  const mergedClass = classesMerge(commonClass, desktop());
  return mergedClass;
};

export default styles;
