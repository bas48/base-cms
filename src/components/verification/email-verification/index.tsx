import { useState } from 'react';
import useCommon from '@utils/hooks/common';
import useRequest from '@utils/hooks/request';
import { Form, Button, Input } from 'antd';
import { useStopwatch } from 'react-timer-hook';
import verificationApi from '@utils/api/verification.api';
import styles from './styles';

interface PropsType {
  form: any;
  onToken: Function;
  isGuest?: boolean;
}

const EmailVerification = (props: PropsType) => {
  const { form, onToken, isGuest = true } = props;
  const { t, classes } = useCommon({
    withStyles: styles,
    withTranslation: ['email-verification'],
  });
  const timer = useStopwatch({ autoStart: false });
  const [sent, setSent] = useState(false);
  const [code, setCode] = useState(null);
  const [token, setToken] = useState(null);
  const resendLimit = 60;

  const { run: sendCode, loading: sendLoading } = useRequest(async (email) => {
    const response = await verificationApi.sendCode('email', email, null, isGuest);
    console.log('Code:', response);
    if (!sent) {
      setSent(true);
    }
    timer.start();
  }, { showError: true });

  const { run: verifyCode, loading: verifyLoading } = useRequest(async (email) => {
    const response = await verificationApi.verifyCode('email', code, email);
    onToken(response);
    setToken(response);
  }, { showError: true, successMessage: t('translation:success') });

  if (timer.minutes === 1) {
    timer.reset();
    timer.pause();
    timer.isRunning = false;
  }

  const reset = () => {
    setToken(null);
    setCode(null);
    setSent(false);
    onToken(null);
    timer.reset();
    form.setFieldsValue({
      verifyCode: null,
    });
  };

  return (
    <>
      <Form.Item
        name="email"
        rules={[
          { type: 'email', message: t('error:invalid_email_pattern') },
          { whitespace: true, message: t('error:please_input', { field: t('email_address') }) },
          { required: true, message: t('error:please_input', { field: t('email_address') }) },
        ]}
      >
        <Input
          placeholder={t('email_address')}
          onChange={() => {
            if (token) {
              reset();
            }
          }}
        />
      </Form.Item>
      {
        token
          ? null
          : (
            <div className={classes.verifyInput}>
              <Form.Item
                className="input"
                name="emailVerifyCode"
                rules={[
                  { required: true, message: t('error:please_input', { field: t('email_verification_code') }) },
                  {
                    validator(rule, value) {
                      if (!value || token) {
                        return Promise.resolve();
                      }
                      return Promise.reject(t('email_not_verify_error'));
                    },
                  },
                ]}
              >
                <Input
                  placeholder={t('email_verification_code')}
                  disabled={!sent}
                  onChange={(e) => setCode(e.target.value)}
                />
              </Form.Item>
              <Button
                className="send-button"
                type="primary"
                loading={sendLoading || verifyLoading}
                disabled={timer.isRunning && !code}
                onClick={async () => {
                  const result = await form.validateFields(['email']);
                  const { email } = result;
                  if (code) {
                    verifyCode(email);
                  } else {
                    sendCode(email);
                  }
                }}
              >
                {
                  (code && t('confirm'))
                  || (timer.isRunning && t('timer_count', { count: resendLimit - timer.seconds }))
                  || (sent && t('resend_verification_code'))
                  || t('send_verification_code')
                }
              </Button>
            </div>
          )
      }
    </>
  );
};

export default EmailVerification;
