import { useState, useEffect } from 'react';
import classnames from 'classnames';
import useCommon from '@utils/hooks/common';
import { Input } from 'antd';
import styles from './styles';

interface propsType {
  className?: any,
  style?: object,
  disabled?: boolean,
  length: number,
  onKeyDown?: Function,
  onChange?: Function,
  onComplete?: Function,
}

const CodeInput = (props: propsType) => {
  const { disabled, className, style, length, onKeyDown, onChange, onComplete } = props;
  const { classes, t } = useCommon({
    withStyles: styles,
    withTranslation: ['verification'],
  });
  const [code, setCode] = useState('');
  useEffect(() => {
    if (onChange && !disabled) {
      onChange(code);
    }
    if (onComplete && code.length === length) {
      onComplete(code);
    }
  }, [code]);

  return (
    <Input
      className={classnames(classes.codeInput, className)}
      onFocus={(e) => e.target.select()}
      style={style}
      autoComplete="off"
      disabled={disabled}
      value={code}
      maxLength={length}
      placeholder={t('input_field', { field: t('code') })}
      onChange={(e) => setCode(e.target.value)}
      onKeyDown={(e) => (onKeyDown ? onKeyDown(e.keyCode) : null)}
    />
  );
};

export default CodeInput;
