import { useEffect, useState } from 'react';
import { Modal, Collapse } from 'antd';
import TwoFAVerification from '@components/verification/two-factor-verification';
import { autorun } from 'mobx';
import useCommon from '@utils/hooks/common';

const { Panel } = Collapse;
const verifyTypes = ['twoFA', 'email', 'phone'];

const VerificationBoard = () => {
  const { t, verification } = useCommon({
    withTranslation: ['verification'],
    observers: ['verification', 'user'],
  });
  const [visible, setVisible] = useState(false);
  const [tokens, setTokens] = useState({});
  const [activeKey, setActiveKey] = useState(verifyTypes);

  useEffect(() => {
    autorun(() => {
      if (verification.tokenTypes) {
        setVisible(true);
      } else {
        setVisible(false);
        setTokens({});
        setActiveKey(verifyTypes);
      }
    });
  }, []);

  const upsertToken = (type, token) => {
    const newTokens = { ...tokens };
    newTokens[`${type}Token`] = token;
    setTokens(newTokens);
  };

  useEffect(() => {
    if (verification.tokenTypes) {
      let allVerify = true;
      for (const tokenType of verification.tokenTypes) {
        if (!tokens[`${tokenType.type}Token`]) {
          allVerify = false;
          break;
        }
      }
      if (allVerify) {
        verification.confirmVerification(tokens);
      }
    }
  }, [tokens]);

  const getContent = (tokenType) => {
    const { type, verifyRequired } = tokenType;
    switch (type) {
      // case 'sms':
      //   return (
      //     <Panel header={t('phone_verification')} key={type} >
      //       <PhoneVerification
      //         defaultValue={reference || phone}
      //         editable={false}
      //         verifyRequired={verifyRequired}
      //         onToken={(token) => {
      //           upsertToken(type, token);
      //           updateState({
      //             activeKey: activeKey.filter((key) => key !== type),
      //           });
      //         }}
      //       />
      //     </Panel>
      //   );
      // case 'email':
      //   return (
      //     <Panel header={t('email_verification')} key={type} >
      //       <EmailVerification
      //         defaultValue={reference || email}
      //         editable={false}
      //         verifyRequired={verifyRequired}
      //         onToken={(token) => {
      //           upsertToken(type, token);
      //           updateState({
      //             activeKey: activeKey.filter((key) => key !== type),
      //           });
      //         }}
      //       />
      //     </Panel>
      //   );
      case 'twoFA':
        return (
          <Panel header={t('two_factor_authentication')} key={type}>
            <TwoFAVerification
              verifyRequired={verifyRequired}
              onToken={(token) => {
                upsertToken(type, token);
                setActiveKey(activeKey.filter((key) => key !== type));
              }}
            />
          </Panel>
        );
      default:
        return null;
    }
  };

  return (
    <Modal
      title={t('verification')}
      visible={visible}
      footer={null}
      onCancel={() => {
        verification.cancelVerification();
      }}
      destroyOnClose
      maskClosable={false}
    >
      <Collapse
        activeKey={activeKey}
      >
        {
          verification.tokenTypes?.map((tokenType) => (
            getContent(tokenType)
          ))
        }
      </Collapse>
    </Modal>
  );
};

export default VerificationBoard;
