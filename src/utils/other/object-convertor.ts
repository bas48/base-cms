export const convertObjToTrueObj = (parentKey, obj) => {
  let trueObj = {};
  for (const key of Object.keys(obj)) {
    const trueKey = parentKey ? `${parentKey}.${key}` : key;
    if (typeof obj[key] === 'object' && obj[key]) {
      const keyObj = convertObjToTrueObj(trueKey, obj[key]);
      trueObj = { ...trueObj, ...keyObj };
    } else {
      trueObj[trueKey] = obj[key];
    }
  }
  return trueObj;
};

export const convertTrueObjToObj = (trueObj) => {
  const obj = {};
  for (const trueKey of Object.keys(trueObj)) {
    const keys = trueKey.split('.');
    let cObj = obj;
    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];
      if (i === keys.length - 1) {
        cObj[key] = trueObj[trueKey];
      } else {
        cObj[key] = cObj[key] || {};
        cObj = cObj[key];
      }
    }
  }
  return obj;
};

export const isEmpty = (value) => {
  if (value || value === false || value === 0) {
    return false;
  }
  return true;
};

export const emptyFilter = (obj) => {
  let returnValue = null;
  if (Array.isArray(obj)) {
    const newArray = [];
    for (let i = 0; i < obj.length; i += 1) {
      const value = emptyFilter(obj[i]);
      if (!isEmpty(value)) {
        newArray.push(value);
      }
    }
    if (newArray.length) {
      returnValue = newArray;
    }
  } else if (typeof obj === 'object' && obj !== null) {
    const newObj = {};
    for (const key of Object.keys(obj)) {
      const value = emptyFilter(obj[key]);
      if (!isEmpty(value)) {
        newObj[key] = value;
      }
    }
    if (Object.keys(newObj).length) {
      returnValue = newObj;
    }
  } else if (!isEmpty(obj)) {
    returnValue = obj;
  }
  return returnValue;
};

export const mergeObject = (obj1, obj2) => {
  const trueObj1 = convertObjToTrueObj(null, obj1);
  const trueObj2 = convertObjToTrueObj(null, obj2);
  const mergeTrueObj = { ...trueObj1, ...trueObj2 };
  return convertTrueObjToObj(mergeTrueObj);
};
