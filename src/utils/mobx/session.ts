import { observable, action, computed } from 'mobx';
import BaseStore from './baseStore';

class SessionStore extends BaseStore {
  @observable accessToken = null;
  @observable refreshToken = null;

  constructor() {
    super();
    if (process.browser) {
      const valueString = localStorage.getItem('session');
      if (valueString) {
        try {
          const value = JSON.parse(valueString);
          this.setSession(value.accessToken, value.refreshToken);
        } catch (e) {
          console.error('parse session json failed', e);
        }
      }
      this.isReady = true;
    }
  }

  @action setSession(accessToken, refreshToken) {
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
    localStorage.setItem('session', JSON.stringify({
      accessToken: this.accessToken,
      refreshToken: this.refreshToken,
    }));
  }

  @action removeSession() {
    this.accessToken = null;
    this.refreshToken = null;
    document.cookie = null;
    localStorage.removeItem('session');
  }

  @action clearCookie() {
    const keys = document.cookie.match(/[^ =;]+(?=)/g);
    if (keys) {
      const expireDate = new Date(0).toUTCString();
      for (let i = keys.length; i >= 0; i -= 1) {
        document.cookie = `${keys[i]}=null;expires=${expireDate}`;
      }
    }
  }

  @computed get isLogin() {
    return !!(this.accessToken && this.refreshToken);
  }
}

export default SessionStore;

export const sessionStore = new SessionStore();
