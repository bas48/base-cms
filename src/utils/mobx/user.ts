import { action, autorun, observable } from 'mobx';
import userApi from '@utils/api/user.api';
import BaseStore from './baseStore';
import SessionStore from './session';

class UserStore extends BaseStore {
  @observable info: any = {};
  sessionStore: SessionStore;

  constructor(sessionStore) {
    super();
    this.isReady = true;
    this.sessionStore = sessionStore;
    autorun(() => {
      if (this.sessionStore.isLogin) {
        this.getPersonalInfo();
      }
    });
  }

  @action async getPersonalInfo() {
    const response = await userApi.getPersonalInfo();
    this.info = response;
  }

  @action async updateTwoFactor(token) {
    const { twoFAEnable } = this.info;
    await userApi.updateTwoFactor(token, !twoFAEnable);
    await this.getPersonalInfo();
  }

  @action async login(userId, password) {
    const response = await userApi.login(userId, password);
    const { accessToken, refreshToken } = response;
    this.sessionStore.setSession(accessToken, refreshToken);
  }

  @action async socialLogin(token, type) {
    const response = await userApi.socialLogin(token, type);
    const { accessToken, refreshToken } = response;
    this.sessionStore.setSession(accessToken, refreshToken);
  }

  @action async logout() {
    await userApi.logout();
    this.sessionStore.removeSession();
    this.sessionStore.clearCookie();
    this.info = {};
  }
}

export default UserStore;
