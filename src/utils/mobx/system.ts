import { observable, action, computed } from 'mobx';
import BaseStore from './baseStore';

export type MessageType = 'success' | 'error' | 'info' | 'warning';

interface Message {
  type: MessageType;
  content: string;
}

interface LoadingEvent {
  id: number;
  message: string;
}

class SystemStore extends BaseStore {
  @observable message: Message = null;
  @observable loadingList: LoadingEvent[] = [];

  constructor() {
    super();
    this.isReady = true;
  }

  @action showMessage(message) {
    this.message = message;
  }

  @action clearMessage() {
    this.message = null;
  }

  @computed get isLoading() {
    return this.loadingList.length > 0;
  }

  @action startLoading(message) {
    const id = this.loadingList.length + 1;
    this.loadingList.push({
      id,
      message,
    });
    return id;
  }

  @action stopLoading(id) {
    let selectedEventIndex;
    for (let i = 0; i < this.loadingList.length; i += 1) {
      if (this.loadingList[i].id === id) {
        selectedEventIndex = i;
        break;
      }
    }
    this.loadingList.splice(selectedEventIndex, 1);
  }
}

export default SystemStore;

export const systemStore = new SystemStore();
