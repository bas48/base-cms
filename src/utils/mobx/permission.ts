import { observable, action, autorun } from 'mobx';
import userApi from '@utils/api/user.api';
import BaseStore from './baseStore';

class PermissionStore extends BaseStore {
  @observable permissions = null;

  constructor(session) {
    super();
    autorun(() => {
      if (session.isLogin) {
        this.isReady = false;
        this.fetchPermissions();
      } else {
        this.setPermissions(null);
        this.isReady = true;
      }
    });
  }

  @action setPermissions(permissions) {
    this.permissions = permissions;
  }

  @action async fetchPermissions() {
    const permissions = await userApi.getUserPermissions();
    this.setPermissions(permissions);
    this.isReady = true;
  }

  countRight(right) {
    switch (right) {
      case 'read':
        return 1;
      case 'write':
        return 2;
      case 'none':
        return 3;
      default:
        return 0;
    }
  }

  comparePermission(requiredPermissions) {
    if (!this.permissions) {
      return {
        success: false,
        needPermission: requiredPermissions,
      };
    }
    const needPermission = {};
    // eslint-disable-next-line
    for (const permissionId of Object.keys(requiredPermissions)) {
      if (!this.permissions[permissionId] || this.permissions[permissionId] === 'none' || this.countRight(this.permissions[permissionId]) < this.countRight(requiredPermissions[permissionId])) {
        needPermission[permissionId] = requiredPermissions[permissionId];
      }
    }
    if (Object.keys(needPermission).length) {
      return {
        success: false,
        needPermission,
      };
    }
    return {
      success: true,
    };
  }
}

export default PermissionStore;
