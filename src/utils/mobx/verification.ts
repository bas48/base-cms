import { observable } from 'mobx';
import { EventEmitter } from 'events';
import BaseStore from './baseStore';

class VerificationStore extends BaseStore {
  @observable tokenTypes;
  events = new EventEmitter();

  constructor() {
    super();
    this.tokenTypes = null;
    this.isReady = true;
  }

  async promptVerification(tokenTypes) {
    this.tokenTypes = tokenTypes;
    return new Promise((res, rej) => {
      let finished = false;
      this.events.once('confirm', (data) => {
        if (!finished) {
          finished = true;
          res(data);
          this.tokenTypes = null;
        }
      });
      this.events.once('cancel', () => {
        if (!finished) {
          finished = true;
          rej(new Error('cancel error'));
          this.tokenTypes = null;
        }
      });
    });
  }

  async confirmVerification(tokens) {
    console.log('Check 2:', tokens);
    this.events.emit('confirm', tokens);
  }

  async cancelVerification() {
    this.events.emit('cancel');
  }
}

export default VerificationStore;

export const verificationStore = new VerificationStore();
