import { useStaticRendering } from 'mobx-react';

import { sessionStore } from './session';
import SystemStore, { systemStore } from './system';
import LanguageStore, { languageStore } from './language';
import PermissionStore from './permission';
import UserStore from './user';
import SocketStore from './socket';
import VerificationStore, { verificationStore } from './verification';

const isServer = typeof window === 'undefined';
useStaticRendering(isServer);

let store = null;

export default function initializeStore(initialData = { language: {} }) {
  const { language } = initialData;
  if (isServer) {
    const serverLanguageStore = new LanguageStore();
    serverLanguageStore.initLanguage(language);
    return {
      session: sessionStore,
      permission: new PermissionStore(sessionStore),
      language: serverLanguageStore,
      user: new UserStore(sessionStore),
      system: new SystemStore(),
      verification: new VerificationStore(),
    };
  }
  if (store === null) {
    languageStore.initLanguage(language);
    store = {
      session: sessionStore,
      permission: new PermissionStore(sessionStore),
      language: languageStore,
      user: new UserStore(sessionStore),
      system: systemStore,
      verification: verificationStore,
      socket: new SocketStore(),
    };
  }
  return store;
}
