import { action } from 'mobx';
import getConfig from 'next/config';
import SocketClient from '@utils/socket';
import BaseStore from './baseStore';

const { publicRuntimeConfig } = getConfig();
const { REACT_URL } = publicRuntimeConfig;

class SocketStore extends BaseStore {
  client;

  constructor() {
    super();
    this.client = new SocketClient(REACT_URL);
    this.isReady = true;
  }

  @action async call(...props) {
    return this.client.call(...props);
  }

  @action async subscribe(...props) {
    return this.client.subscribe(...props);
  }

  @action async disconnect() {
    return this.client.close();
  }

  @action async testRes(...props) {
    return this.client.testRes(...props);
  }
}

export default SocketStore;
