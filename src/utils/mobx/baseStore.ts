import { observable } from 'mobx';

class BaseStore {
  @observable isReady = false;
}

export default BaseStore;
