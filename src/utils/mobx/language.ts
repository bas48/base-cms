import { observable, action } from 'mobx';
import BaseStore from './baseStore';

class LanguageStore extends BaseStore {
  @observable supportLanguages = [];
  @observable currentLanguage = 'en-US';

  @action initLanguage(initStore) {
    const { supportLanguages, currentLanguage } = initStore;
    this.supportLanguages = supportLanguages;
    this.setLanguage(currentLanguage);
    this.isReady = true;
  }

  @action setLanguage(languageId) {
    if (this.getLanguage(languageId)) {
      this.currentLanguage = languageId;
    }
  }

  getLanguage(languageId) {
    const languages = [...this.supportLanguages];
    for (let i = 0; i < languages.length; i += 1) {
      if (languages[i].id === languageId) {
        return languages[i];
      }
    }
    return null;
  }
}

export default LanguageStore;

export const languageStore = new LanguageStore();
