const merge = (target, source) => {
  if (typeof target === 'undefined') {
    return source;
  }
  if (typeof source === 'undefined') {
    return target;
  }
  // Iterate through `source` properties and if an `Object` set property to merge of `target` and `source` properties
  for (const key of Object.keys(source)) {
    if (source[key] instanceof Object) Object.assign(source[key], merge(target[key], source[key]));
  }

  // Join `target` and modified `source`
  const newTarget = {
    ...target,
  };
  Object.assign(newTarget || {}, source);
  return newTarget;
};

const theme = {
  colorPrimary: 'green',
  contentBackground: 'white',
  styleFormat: [
    {
      id: 'desktop',
      width: 1920,
      changePoint: 1024,
    },
    {
      id: 'mobile',
      width: 375,
      changePoint: 0,
    },
  ],
};

export const getCurrentStyleFormat = (screenWidth) => {
  const { styleFormat } = theme;
  for (let i = 0; i < styleFormat.length; i += 1) {
    const { changePoint } = styleFormat[i];
    if (screenWidth > changePoint) {
      return {
        ...styleFormat[i],
        screenWidth,
      };
    }
  }
  return null;
};

interface Options {
  withUnit?: boolean;
  withLimit?: boolean;
}

export const getTheme = (styleFormat) => {
  const toVW = (size, options?: Options) => {
    const { withUnit, withLimit = true } = options || {};
    if (styleFormat) {
      const { width, screenWidth } = styleFormat;
      if (screenWidth < width || !withLimit) {
        return `${(size / styleFormat.width) * 100}vw`;
      }
    }
    return withUnit ? `${size}px` : size;
  };

  const commonClass = {
    '@font-face': [
      {
        fontFamily: 'TestFront',
        src: 'url(/font/test.otf)',
      },
    ],
  };

  return {
    ...theme,
    toVW,
    commonClass,
    classesMerge: merge,
    currentStyleFormat: styleFormat,
  };
};
