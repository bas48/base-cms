import { io, Socket } from 'socket.io-client';

interface SubscribeParams {
  event: string;
  parameters?: object;
}

class SocketClient {
  path: string;
  socket: Socket;
  on: Function;
  constructor(path: string) {
    this.path = path;
    this.socket = io(path, {
      reconnection: true,
      reconnectionDelay: 1000,
      reconnectionDelayMax: 8000,
      reconnectionAttempts: 10000000,
    });
    this.on = this.socket.on;
  }

  waitUntilConnected = async () => {
    if (this.socket.connected) {
      return Promise.resolve();
    }
    return new Promise((res) => this.socket.once('connect', res));
  };

  call = async (event, params?: object) => {
    await this.waitUntilConnected();
    return new Promise((res, rej) => {
      this.socket.emit(event, params || {}, (response) => {
        console.log('responsesss: ', response);
        if (response.status === 200) {
          res(response.data);
        } else {
          rej(response.data);
        }
      });
    });
  };

  subscribe = async (event, parameters, callBack) => {
    const params: SubscribeParams = { event };
    if (parameters) {
      params.parameters = parameters;
    }
    const response = await this.call('/v1/channel/subscribe', params);
    if (response && callBack) {
      callBack(response);
    }
    this.socket.on(event, callBack);
    return async () => {
      this.socket.off(event, callBack);
      const res = await this.call('/v1/channel/unsubscribe', { event });
      return res;
    };
  };

  testRes = async (callBack: Function) => {
    this.socket.on('testRes', (data) => callBack(data));
  };
}

export default SocketClient;
