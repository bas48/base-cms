/* eslint-disable array-callback-return */
/* eslint-disable no-loop-func */
/* eslint-disable no-case-declarations */
import { useTranslation } from 'react-i18next';
import { createUseStyles, useTheme } from 'react-jss';
import { Classes } from 'jss';
import { TFunction } from 'i18next';
import { useObservers, useObserversFn } from '@utils/hooks/observers';

interface DecoratorDefine {
  withStyles?: any,
  withTranslation?: string[],
  observers?: any,
  props ?: any,
}

const useCommon = (define: DecoratorDefine): {
  classes?: Classes<string>;
  theme?: Object;
  t?: TFunction;
  [key: string]: any;
} => {
  let commonHooks: any = {};
  const { props } = define;
  for (const decorator of Object.keys(define)) {
    const element = define[decorator];
    switch (decorator) {
      case 'withStyles':
        const useStyles = createUseStyles<string>(element);
        const theme = useTheme();
        const classes = useStyles({ props, theme });
        commonHooks.classes = classes;
        commonHooks.theme = theme;
        break;
      case 'withTranslation':
        const { t } = element ? useTranslation(...element) : useTranslation();
        commonHooks.t = t;
        break;
      case 'observers':
        let observers = {};
        element.map((item) => {
          switch (typeof item) {
            case 'string':
              observers = {
                ...observers,
                ...useObservers(item),
              };
              break;
            case 'function':
              observers = {
                ...observers,
                ...useObserversFn(item),
              };
              break;

            default:
              break;
          }
        });
        commonHooks = {
          ...commonHooks,
          ...observers,
        };

        break;
      default:
        break;
    }
  }
  return commonHooks;
};

export default useCommon;
