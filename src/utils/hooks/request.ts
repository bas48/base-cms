import { useState } from 'react';
import { systemStore } from '@utils/mobx/system';

interface RequestOption {
  showError: boolean,
  successMessage ?: string;
  onError ?: Function;
}

const useRequest = (requestFunc: Function, option?: RequestOption) => {
  const returnParam: any = {};
  const { showError, successMessage } = option;
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const onError = (e) => {
    if (e.response?.data && e.message) {
      // eslint-disable-next-line no-param-reassign
      e = e.response.data;
    } else {
      console.log(e);
    }
    let errorMsg = e.message;
    const { extras } = e;
    if (extras && typeof extras === 'string') {
      errorMsg += `: ${extras}`;
    } else if (typeof extras === 'object') {
      const { errors } = extras;
      if (errors) {
        for (const extraError of errors) {
          if (typeof extraError === 'string') {
            errorMsg += `: ${extraError}`;
          }
        }
      }
    }
    systemStore.showMessage({
      type: 'error',
      content: errorMsg,
    });
  };
  const run = async (...funcParams) => {
    setLoading(true);
    let response;
    try {
      response = await requestFunc(...funcParams);
      if (successMessage) {
        systemStore.showMessage({
          type: 'success',
          content: successMessage,
        });
      }
    } catch (e) {
      if (showError) {
        onError(e);
      }
      setError(e);
      if (option.onError) {
        option.onError(e);
      }
    }
    setLoading(false);
    return response;
  };
  if (error) {
    returnParam[error] = error;
    setError(null);
  }
  returnParam.loading = loading;
  returnParam.run = run;
  return returnParam;
};

export default useRequest;
