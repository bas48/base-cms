import React from 'react';
import { MobXProviderContext, useObserver } from 'mobx-react';

const useStores = () => React.useContext(MobXProviderContext);

export const useObservers = (...keys) => {
  const stores = useStores();
  const observer = {};
  for (const key of keys) {
    observer[key] = stores[key];
  }
  return useObserver(() => (observer));
};

export const useObserversFn = (fn: Function): object => {
  const stores = useStores();
  const observer = fn(stores);
  return useObserver(observer);
};
