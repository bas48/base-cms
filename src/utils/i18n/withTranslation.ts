import { withTranslation as oldWithTranslation } from 'react-i18next';
import hoistNonReactStatic from 'hoist-non-react-statics';

const withTranslation = (type?: any[]) => (component: any) => hoistNonReactStatic(oldWithTranslation(type)(component), component);

export default withTranslation;
