import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';

export const setLanguage = (language, resources) => {
  const i18n = i18next.createInstance();
  i18n
    .use(initReactI18next)
    .init({
      lng: language,
      debug: false,
      resources,
      interpolation: {
        escapeValue: false,
      },
      react: {
        useSuspense: false,
      },
    }, (err) => {
      if (err) {
        console.log(err);
      }
    });
  return i18n;
};

export default setLanguage;
