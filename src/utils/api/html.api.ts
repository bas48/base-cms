import client from './index';

export default {
  async uploadImage(image) {
    const formData = new FormData();
    formData.append('file', image);
    const response = await client.post('/v1/file/html-image', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
    return response.data;
  },
};
