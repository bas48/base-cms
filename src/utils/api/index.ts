import axios from 'axios';
import getConfig from 'next/config';
import { sessionStore } from '@utils/mobx/session';
import { languageStore } from '@utils/mobx/language';
import { verificationStore } from '@utils/mobx/verification';

const { publicRuntimeConfig } = getConfig();
const { REACT_URL } = publicRuntimeConfig;

export const baseURL = REACT_URL;
const client = axios.create({
  baseURL,
});

const getErrorData = (apiError) => {
  const { response } = apiError;
  if (response) {
    const { data } = response;
    return data;
  }
  return null;
};

const refreshTokenManager = {
  isTokenRefreshing: false,
  refreshTokenQueue: [],
  async broadcastRefreshFinish(err) {
    refreshTokenManager.isTokenRefreshing = false;
    const queue = refreshTokenManager.refreshTokenQueue;
    refreshTokenManager.refreshTokenQueue = [];

    if (err) {
      queue.forEach((a) => a.rej(err));
    } else {
      queue.forEach((a) => a.res());
    }
  },
  async waitRefreshToken() {
    return new Promise((res, rej) => {
      refreshTokenManager.refreshTokenQueue.push({ res, rej });
    });
  },
  async startRefreshToken(refreshToken) {
    try {
      refreshTokenManager.isTokenRefreshing = true;
      const response = await axios.patch(`${baseURL}/v1/user/refresh-token`, {
        refreshToken,
      });
      const { accessToken } = response.data;
      sessionStore.setSession(accessToken, refreshToken);
      refreshTokenManager.broadcastRefreshFinish(null);
    } catch (errorData) {
      refreshTokenManager.broadcastRefreshFinish(errorData);
    }
  },
};

const interceptConfig = (requestConfig) => {
  const config = { ...requestConfig };

  if (process.browser) {
    const { accessToken, refreshToken } = sessionStore;
    if (accessToken && refreshToken) {
      config.refreshToken = refreshToken;
      config.headers = requestConfig.headers || {};
      config.headers.Authorization = accessToken;
    }
  }
  config.params = requestConfig.params || {};
  config.params.languageId = languageStore.currentLanguage;
  config.withCredentials = true;
  return config;
};

const resetSession = () => {
  sessionStore.removeSession();
  sessionStore.clearCookie();
};

client.interceptors.request.use(interceptConfig);

client.interceptors.response.use((response) => response,
  async (apiError) => {
    const errorData = getErrorData(apiError);
    if (errorData) {
      const { code, extras } = errorData;
      if (code === 'AccessTokenExpired') {
        if (!refreshTokenManager.isTokenRefreshing) {
          const { refreshToken } = sessionStore;
          refreshTokenManager.startRefreshToken(refreshToken);
        }
        try {
          await refreshTokenManager.waitRefreshToken();
        } catch (e) {
          resetSession();
          throw e;
        }
        return axios(interceptConfig(apiError.config));
      }
      if (code === 'NotFoundError' && extras?.type === 'session') {
        resetSession();
      }

      if (code === 'VerificationRequired') {
        const { missingToken } = extras;
        try {
          const data = await verificationStore.promptVerification(missingToken);
          // eslint-disable-next-line
          apiError.config.params = data || {};
          return axios(interceptConfig(apiError.config));
        } catch (e) {
          return Promise.reject(apiError);
        }
      }
    }
    return Promise.reject(apiError);
  });

export default client;
