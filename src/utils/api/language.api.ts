import client from './index';

export default {
  async getLanguages() {
    const response = await client.get('/v1/language');
    return response.data;
  },
};
