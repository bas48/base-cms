import client from './index';

export default {
  async getTranslationSource(platform: string) {
    const response = await client.get('/v1/translation/source', {
      params: {
        platform,
      },
    });
    return response.data;
  },
};
