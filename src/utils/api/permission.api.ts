import client from './index';

export default {
  async getPermissions() {
    const response = await client.get('/v1/permission');
    return response.data;
  },
  async getUserPermissions(userId) {
    const response = await client.get(`/v1/permission/user/${userId}`);
    return response.data;
  },
  async assignUserPermissions(userId, permissions) {
    const response = await client.patch(`/v1/permission/user/${userId}`, {
      permissions,
    });
    return response.data;
  },
  async getRolePermissions(roleId) {
    const response = await client.get(`/v1/permission/role/${roleId}`);
    return response.data;
  },
  async assignRolePermissions(roleId, permissions) {
    const response = await client.patch(`/v1/permission/role/${roleId}`, {
      permissions,
    });
    return response.data;
  },
};
