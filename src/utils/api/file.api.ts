import client from './index';

export default {
  async uploadTemporaryFile(file, type) {
    const formData = new FormData();
    formData.append('file', file);
    let path = 'temporary';
    switch (type) {
      case 'image':
        path = 'temporary/image';
        break;
      case 'video':
        path = 'temporary/video';
        break;
      default:
        break;
    }
    const response = await client.post(`/v1/file/${path}`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
    return response.data;
  },
  async uploadHtmlImage(file) {
    const formData = new FormData();
    formData.append('file', file);
    const response = await client.post('/v1/file/html-image', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
    return response.data;
  },
};
