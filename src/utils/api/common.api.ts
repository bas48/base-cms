import { emptyFilter } from '@utils/other/object-convertor';
import client from './index';

interface ListParams {
  page: number;
  sort: string;
  limit: number;
  search?: string;
  filter?: object;
  exportData?: boolean;
}

interface SortParam {
  orderBy: string,
  direction: string,
}

export default {
  async getSelectorItems(path: string) {
    const response = await client.get(path);
    return response.data;
  },

  async getList(path: string, page: number, sort: SortParam[], limit: number, search?: string, filter?: object, exportData?: boolean) {
    const listParam: ListParams = { page, sort: JSON.stringify(sort), limit, exportData };
    if (search) {
      listParam.search = search;
    }
    const newFilter = emptyFilter(filter);
    if (newFilter) {
      listParam.filter = filter;
    }
    const response = await client.get(`${path}/list`, {
      params: listParam,
    });
    return response.data;
  },

  async createRecord(path: string, data: any) {
    const response = await client.post(path, data);
    return response.data;
  },

  async updateRecord(path: string, recordId: string, data: any) {
    const response = await client.patch(`${path}/${recordId}`, data);
    return response.data;
  },

  async deleteRecord(path: string, recordId: string) {
    const response = await client.delete(`${path}/${recordId}`);
    return response.data;
  },
};
