import client from './index';

export default {
  async getDev() {
    const response = await client.get('/dev/dev');
    return response.data;
  },
  async postDev() {
    const response = await client.post('/dev/dev');
    return response.data;
  },
};
