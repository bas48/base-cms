import client from './index';

export default {
  async getMessagesHandler() {
    const response = await client.get('/v1/message/handler');
    return response.data;
  },
};
