import client from './index';

interface UserInfo {
  oldPassword: string;
  newPassword: string;
}

export default {
  async getPersonalInfo() {
    const response = await client.get('/v1/user/personal');
    return response.data;
  },
  async changePersonalInfo(data: UserInfo) {
    const response = await client.patch('/v1/user/personal', data);
    return response.data;
  },
  async login(userId: string, password: string) {
    const response = await client.post('/v1/user/login', {
      userId, password,
    });
    return response.data;
  },
  async logout() {
    const response = await client.delete('/v1/user/logout');
    return response.data;
  },
  async getUserPermissions() {
    const response = await client.get('/v1/user/permissions');
    return response.data;
  },

  async resetPassword(token: string, password: string) {
    const response = await client.patch('/v1/user/reset-password', {
      token, password,
    });
    return response.data;
  },
  async getTwoFAQrCode() {
    const response = await client.post('/v1/user/two-factor');
    return response.data;
  },

  async updateTwoFactor(token: string, enable: boolean) {
    const response = await client.patch('/v1/user/two-factor', {
      token, enable,
    });
    return response.data;
  },
  async socialLogin(token: string, type: string) {
    const response = await client.post('/v1/user/social/login', {
      token, type,
    });
    return response.data;
  },
};
