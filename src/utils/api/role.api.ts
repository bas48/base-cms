import client from './index';

export default {
  async assignUserRoles(userId, roles) {
    const response = await client.patch(`/v1/role/user/${userId}`, {
      roles,
    });
    return response.data;
  },
};
