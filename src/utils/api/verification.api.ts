import client from './index';

type Type = 'email' | 'phone';

export default {
  async sendCode(type: Type, email?: string, phone?: string, isGuest: boolean = false, isCall: boolean = false) {
    const body: {
      type: Type,
      isGuest: boolean,
      isCall: boolean,
      email?: string,
      phone?: string,
    } = { type, isGuest, isCall };
    if (type === 'email') {
      body.email = email;
    } else if (type === 'phone') {
      body.phone = phone;
    }
    const response = await client.post('/v1/verification/send', body);
    return response.data;
  },
  async verifyCode(type: Type, code: string, email?: string, phone?: string) {
    const body: {
      type: Type,
      code: string,
      email?: string,
      phone?: string,
    } = { type, code };
    if (type === 'email') {
      body.email = email;
    } else if (type === 'phone') {
      body.phone = phone;
    }
    const response = await client.post('/v1/verification/verify', body);
    return response.data;
  },
  async createTwoFARecord(twoFACode: string) {
    const response = await client.post('/v1/verification/twoFA', {
      twoFACode,
    });
    return response.data;
  },
};
