import { Select, DatePicker } from 'antd';
import useCommon from '@utils/hooks/common';
import styles from './styles';

const { Option } = Select;

const IndexPage = () => {
  const { classes, t, language } = useCommon({
    withTranslation: null,
    withStyles: styles,
    observers: ['language'],
  });
  const { currentLanguage, supportLanguages } = language;

  return (
    <div className={classes.main}>
      <div>{t('theme')}</div>
      {
        supportLanguages
          ? (
            <Select value={currentLanguage} onChange={(languageId) => language.setLanguage(languageId)}>
              {supportLanguages.map((lang) => {
                const { id, name } = lang;
                return (
                  <Option key={id} value={id}>{name}</Option>
                );
              })}
            </Select>
          ) : null
      }
      <DatePicker />
    </div>
  );
};

export default IndexPage;
