import { useState, useEffect } from 'react';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import moment from 'moment';
import { Button, DatePicker, Input, Popconfirm, Row, Col } from 'antd';
import messageApi from '@utils/api/message.api';
import ListPage from '@components/list-page';
import BaseForm from '@components/base-form';
import FileUpload from '@components/file-upload';
import DataSelector from '@components/data-selector';
import useCommon from '@utils/hooks/common';
import { DateRange } from '@interface';
import styles from './styles';

const { TextArea } = Input;
const { RangePicker } = DatePicker;

interface FilterData {
  updatedAt?: DateRange;
  createdAt?: DateRange;
}

const MessageManagement = () => {
  const { t, language } = useCommon({
    withStyles: styles,
    withTranslation: ['message-management'],
    observers: ['language'],
  });
  const { currentLanguage } = language;

  const [state, setState] = useState({
    handlers: [],
    handlerVariables: [],
  });
  const updateState = (updatedData) => {
    setState((prevState) => ({ ...prevState, ...updatedData }));
  };

  useEffect(() => {
    const getHandlers = async () => {
      const handlers = await messageApi.getMessagesHandler();
      updateState({ handlers });
    };
    getHandlers();
  }, []);

  const updateHandlerVariables = (handlerIds) => {
    if (handlerIds?.length) {
      const { handlers } = state;
      const handlerVariables = [];
      const selectedHandlerIds = handlers.length ? handlerIds : [];
      for (const selectedHandlerId of selectedHandlerIds) {
        const selectedHandler = handlers.find((handler) => handler.id === selectedHandlerId);
        if (selectedHandler?.returnKeys) {
          for (const handlerReturn of selectedHandler.returnKeys) {
            handlerVariables.push(`${selectedHandlerId}_${handlerReturn}`);
          }
        }
      }
      updateState({ handlerVariables });
    } else {
      updateState({ handlerVariables: [] });
    }
  };

  const getFormItems = (isCreate) => {
    const { handlers } = state;
    const items: any = [];
    if (isCreate) {
      items.push({
        name: 'id',
        label: t('message_id'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('message_id') }),
          },
        ],
        render: () => (
          <Input />
        ),
      });
    }
    items.push(...[

      {
        name: 'title',
        label: t('title'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('title') }),
          },
        ],
        isTranslation: true,
        render: () => (
          <Input />
        ),
      },
      {
        name: 'content',
        label: t('content'),
        isTranslation: true,
        render: () => (
          <TextArea
            autoSize={{ maxRows: 5 }}
          />
        ),
      },
      {
        name: 'imageContent',
        label: t('image_content'),
        isTranslation: true,
        render: () => (
          <FileUpload
            limit={1}
            sizeLimit={1}
            accept="image/*"
          />
        ),
      },
    ]);
    if (handlers.length) {
      items.push({
        name: 'handler',
        label: t('handler'),
        render: () => (
          <DataSelector
            mode="multiple"
            dataSource={handlers}
            defineLabel={(option) => option.title}
            defineValue={(option) => option.id}
            onChange={(value) => updateHandlerVariables(value)}
          />
        ),
      });
    }
    return items;
  };

  const { handlerVariables } = state;
  return (
    <ListPage
      title={t('message_management')}
      apiPath="/v1/message"
      searchInfo={`${t('title')} ${t('translation:or')} ${t('content')}`}
      createBoard={(onCreate, loading) => (
        <>
          <span>{`${t('handler_variables')}: ${handlerVariables.join(', ')}`}</span>
          <BaseForm
            loading={loading}
            formProps={{
              labelCol: { span: 6 },
              wrapperCol: { span: 18 },
            }}
            formItems={() => getFormItems(true)}
            onSubmit={(value) => onCreate(value)}
          />
        </>
      )}
      onCreateBoardClose={() => updateHandlerVariables(null)}
      updateBoard={(selectedRecord, onUpdate, loading) => (
        <>
          <span>{`${t('handler_variables')}: ${handlerVariables.join(', ')}`}</span>
          <BaseForm
            loading={loading}
            formProps={{
              labelCol: { span: 6 },
              wrapperCol: { span: 18 },
            }}
            initialValues={{
              ...selectedRecord,
              handler: (selectedRecord?.handlers) || undefined,
            }}
            formItems={() => getFormItems(false)}
            onSubmit={(value, changedValue) => onUpdate(selectedRecord.id, changedValue)}
          />
        </>
      )}
      onUpdateBoardClose={() => updateHandlerVariables(null)}
      filter={(onFilterSubmit, onFilterClear, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          }}
          formItems={() => ([
            {
              // dataIndex: 'updatedAt',
              name: 'updatedAt',
              label: t('updated_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
            {
              // dataIndex: 'createdAt',
              name: 'createdAt',
              label: t('created_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
          ])}
          onSubmit={(value: any) => {
            const { updatedAt, createdAt } = value;
            const filterData: FilterData = { ...value };
            if (updatedAt?.length) {
              filterData.updatedAt = {
                from: updatedAt[0],
                to: updatedAt[1],
              };
            } else {
              delete filterData.updatedAt;
            }
            if (createdAt?.length) {
              filterData.createdAt = {
                from: createdAt[0],
                to: createdAt[1],
              };
            } else {
              delete filterData.createdAt;
            }
            onFilterSubmit(filterData);
          }}
          onClear={() => onFilterClear()}
        />
      )}
      columns={(showUpdate, onDelete, loading) => ([
        {
          title: t('message_id'),
          dataIndex: 'id',
          sorter: true,
        },
        {
          title: t('title'),
          dataIndex: 'title',
          sorter: true,
          render: (title) => title?.[currentLanguage]?.value,
        },
        {
          title: t('system_message'),
          dataIndex: 'isSystem',
          sorter: true,
          render: (isSystem) => {
            if (isSystem) {
              return t('translation:yes');
            }
            return t('translation:no');
          },
        },
        {
          title: t('handler'),
          dataIndex: 'handlers',
          sorter: true,
          render: (handler) => handler?.join(','),
        },
        {
          title: t('updated_at'),
          dataIndex: 'updatedAt',
          render: (updatedAt) => moment(updatedAt).format('YYYY-MM-DD HH:mm:ss'),
          sorter: true,
        },
        {
          title: t('created_at'),
          dataIndex: 'createdAt',
          render: (createdAt) => moment(createdAt).format('YYYY-MM-DD HH:mm:ss'),
          sorter: true,
        },
        {
          title: t('action'),
          render: (record) => (
            <Row gutter={10}>
              <Col>
                <Button
                  onClick={() => {
                    console.log(record);
                    showUpdate(record);
                    if (record) {
                      updateHandlerVariables(record.handlers);
                    }
                  }}
                >
                  <EditOutlined />
                </Button>
              </Col>
              <Col>
                <Popconfirm
                  title={t('delete_record_confirm_msg')}
                  onConfirm={() => onDelete(record.id)}
                  okText={t('yes')}
                  cancelText={t('no')}
                >
                  <Button loading={loading}>
                    <DeleteOutlined />
                  </Button>
                </Popconfirm>
              </Col>
            </Row>
          ),
        },
      ])}
    />
  );
};

export default MessageManagement;
