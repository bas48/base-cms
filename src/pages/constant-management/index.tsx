import moment from 'moment';
import { Button, DatePicker, Input } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import ListPage from '@components/list-page';
import BaseForm, { FormItem } from '@components/base-form';
import TypeInput from '@components/type-input';
import useCommon from '@utils/hooks/common';
import { DateRange } from '@interface';
import styles from './styles';

const { RangePicker } = DatePicker;

interface FilterData {
  updatedAt?: DateRange;
  createdAt?: DateRange;
}

const ConstantManagement = () => {
  const { t, language } = useCommon({
    withTranslation: ['constant-management'],
    withStyles: styles,
    observers: ['language'],
  });
  const { currentLanguage } = language;

  const getFormItems = (form, isCreate, selectedRecord) => {
    const items: FormItem[] = [
      {
        name: 'value',
        label: t('value'),
        rules: [
          {
            required: true,
            type: selectedRecord.type === 'boolean' ? 'string' : selectedRecord.type,
            message: t('error:please_input', { field: t('value') }),
          },
        ],

        render: () => (
          <TypeInput type={selectedRecord.type} />
        ),
      },
      {
        name: 'title',
        label: t('title'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('title') }),
          },
        ],
        isTranslation: true,
        render: () => (
          <Input />
        ),
      },
    ];
    return items;
  };

  return (
    <ListPage
      title={t('constant_management')}
      apiPath="/v1/constant"
      defaultState={{ sort: [{ orderBy: 'title', direction: 'ASC' }] }}
      searchInfo={`${t('key')} ${t('translation:or')} ${t('title')}`}
      updateBoard={(selectedRecord, onUpdate, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          }}
          initialValues={selectedRecord}
          formItems={(form) => getFormItems(form, false, selectedRecord)}
          onSubmit={(value, changedValue) => {
            const updatedValue = { ...changedValue };
            if (updatedValue.value) {
              updatedValue.value = String(updatedValue.value);
            }
            onUpdate(selectedRecord.key, updatedValue);
          }}
        />
      )}
      filter={(onFilterSubmit, onFilterClear, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          }}
          formItems={() => ([
            {
              // dataIndex: 'updatedAt',
              name: 'updatedAt',
              label: t('updated_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
            {
              // dataIndex: 'createdAt',
              name: 'createdAt',
              label: t('created_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
          ])}
          onSubmit={(value: any) => {
            const { updatedAt, createdAt } = value;
            const filterData: FilterData = { ...value };
            if (updatedAt?.length) {
              filterData.updatedAt = {
                from: updatedAt[0],
                to: updatedAt[1],
              };
            } else {
              delete filterData.updatedAt;
            }
            if (createdAt?.length) {
              filterData.createdAt = {
                from: createdAt[0],
                to: createdAt[1],
              };
            } else {
              delete filterData.createdAt;
            }
            onFilterSubmit(filterData);
          }}
          onClear={() => onFilterClear()}
        />
      )}
      columns={(showUpdate) => ([
        {
          title: t('title'),
          dataIndex: 'title',
          sorter: true,
          render: (title) => title?.[currentLanguage]?.value,
        },
        {
          title: t('key'),
          dataIndex: 'key',
          sorter: true,
          render: (key, record) => record.key,
        },
        {
          title: t('value'),
          dataIndex: 'value',
        },
        {
          title: t('datatype'),
          dataIndex: 'type',
          sorter: true,
          render: (type) => t(`datatype:${type}`),
        },
        {
          title: t('updated_at'),
          dataIndex: 'updatedAt',
          render: (updatedAt) => moment(updatedAt).format('YYYY-MM-DD HH:mm:ss'),
          sorter: true,
        },
        {
          title: t('created_at'),
          dataIndex: 'createdAt',
          render: (createdAt) => moment(createdAt).format('YYYY-MM-DD HH:mm:ss'),
          sorter: true,
        },
        {
          title: t('action'),
          render: (record) => (
            <Button
              onClick={() => showUpdate(record)}
            >
              <EditOutlined />
            </Button>
          ),
        },
      ])}
    />
  );
};

export default ConstantManagement;
