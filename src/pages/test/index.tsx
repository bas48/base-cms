import HtmlEditor from '@components/html-editor';
import useCommon from '@utils/hooks/common';
import { Button } from 'antd';
import commonApi from '@utils/api/test.api';
import FacebookLogin from '@components/social-login/facebook-login';
import GoogleLogin from '@components/social-login/google-login';
import styles from './styles';

const TestPage = () => {
  const { classes } = useCommon({
    withStyles: styles,
    withTranslation: ['test'],
  });

  return (
    <div className={classes.testPage}>
      <Button onClick={() => commonApi.getDev()}>Test dev</Button>
      <HtmlEditor />
      <FacebookLogin />
      <GoogleLogin />
    </div>
  );
};

export default TestPage;
