const desktop = (theme) => {
  const { toVW } = theme;
  return {
    testPage: {
      height: '100%',
    },
    testFront: {
      fontFamily: 'TestFront',
      fontSize: toVW(40),
      color: 'red',
    },
  };
};

const mobile = (theme) => {
  const { toVW } = theme;
  return {
    testPage: {
      height: '100%',
    },
    testFront: {
      fontSize: toVW(30),
      color: 'blue',
    },
  };
};

const styles = (theme) => {
  const { currentStyleFormat, commonClass, classesMerge } = theme;
  let mergedClass = classesMerge(commonClass, desktop(theme));
  if (currentStyleFormat?.id === 'mobile') {
    mergedClass = classesMerge(mergedClass, mobile(theme));
  }
  return mergedClass;
};

export default styles;
