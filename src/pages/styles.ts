const desktop = (theme) => {
  const { toVW } = theme;
  return {
    main: {
      height: '100vh',
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      fontSize: toVW(30),
      color: 'black',
    },
  };
};

const mobile = (theme) => {
  const { toVW } = theme;
  return {
    main: {
      fontSize: toVW(12),
      color: 'red',
    },
  };
};

const styles = (theme) => {
  const { currentStyleFormat, commonClass, classesMerge } = theme;
  let mergedClass = classesMerge(commonClass, desktop(theme));
  if (currentStyleFormat?.id === 'mobile') {
    mergedClass = classesMerge(mergedClass, mobile(theme));
  }
  return mergedClass;
};

export default styles;
