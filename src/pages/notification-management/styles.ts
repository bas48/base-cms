const styles = {
  paramValueSelect: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignContent: 'center',
    '& .selector': {
      marginRight: 10,
      width: 'fit-content',
    },
  },
};

export default styles;
