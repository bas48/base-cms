import { useState, useEffect } from 'react';
import moment from 'moment';
import { Button, DatePicker, Input, Popconfirm, Row, Col } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import ListPage from '@components/list-page';
import BaseForm, { FormItem } from '@components/base-form';
import DataSelector from '@components/data-selector';
import MessageSelector from '@components/message-selector';
import NotificationTargetSelector from '@components/notification-target-selector';
import TypeInput from '@components/type-input';
import useCommon from '@utils/hooks/common';
import { DateRange } from '@interface';
import styles from './styles';

const { RangePicker } = DatePicker;

interface FilterData {
  updatedAt?: DateRange,
  createdAt?: DateRange,
}

interface ParamProps {
  type: string;
  value?: any;
  onChange?: Function;
  options?: any;
}

const ParamValueSelect = (props: ParamProps) => {
  const { t, classes } = useCommon({
    withTranslation: ['notification-management.param-value-select'],
    withStyles: styles,
  });
  const { type, options, value, onChange } = props;
  const [selectedType, setSelectedType] = useState(value === 'system' ? value : 'customize');

  return (
    <div className={classes.paramValueSelect}>
      <DataSelector
        value={selectedType}
        className="selector"
        dataSource={[
          {
            label: t('customize'),
            value: 'customize',
          },
          {
            label: t('system_provide'),
            value: 'system',
          },
        ]}
        defineLabel={(option) => option.label}
        defineValue={(option) => option.value}
        onChange={(response) => {
          setSelectedType(response);
          if (response === 'system' && onChange) {
            onChange(response);
          } else {
            onChange(null);
          }
        }}
      />
      {
        selectedType === 'customize'
          ? (
            <TypeInput
              type={type}
              options={options}
              value={value}
              onChange={(response) => {
                if (onChange) {
                  onChange(response);
                }
              }}
            />
          )
          : null
      }
    </div>
  );
};

const NotificationManagement = () => {
  const { t, language } = useCommon({
    withTranslation: ['notification-management'],
    withStyles: styles,
    observers: ['language'],
  });
  const { currentLanguage } = language;
  const [state, setState] = useState({
    contentForms: [],
    contentTypes: {},
  });
  const updateState = (updatedData) => {
    setState((prevState) => ({ ...prevState, ...updatedData }));
  };

  const updateContentForm = () => {
    const { contentTypes } = state;
    let contentForms = [];
    const selectedTypes = Object.keys(contentTypes);
    if (selectedTypes.length) {
      contentForms = selectedTypes.map((type) => {
        const paramsItems = contentTypes[type] || [];
        const items = [
          {
            name: 'message.id',
            label: t('message'),
            rules: [
              {
                required: true,
                validator: (rule, value, callback) => {
                  if (!value) {
                    callback(t('error:please_input', { field: t('message') }));
                  }
                  callback();
                },
              },
            ],
            render: () => (
              <MessageSelector
                onHandlerParams={(handlerParams) => {
                  const newContentTypes = { ...state.contentTypes };
                  newContentTypes[type] = handlerParams;
                  updateState({ contentTypes: newContentTypes });
                }}
              />
            ),
          },
        ];
        for (const paramsItem of paramsItems) {
          const { handler, index, dataType, options } = paramsItem;
          const itemLabel = `${handler.id}-${index}`;
          items.push({
            name: `message.paramsValue.${handler.id}.${index}`,
            label: itemLabel,
            rules: [
              {
                required: true,
                validator: (rule, value, callback) => {
                  if (!value) {
                    callback(t('error:please_input', { field: itemLabel }));
                  }
                  callback();
                },
              },
            ],
            render: () => (
              <ParamValueSelect type={dataType} options={options} />
            ),
          });
        }
        return {
          label: t(`notification_content_type_${type}`),
          index: `content.${type}`,
          items,
        };
      });
    }
    updateState({ contentForms });
  };

  useEffect(() => {
    updateContentForm();
  }, [state.contentTypes]);

  const getFormItems = () => {
    const items: FormItem[] = [
      {
        name: 'title',
        label: t('title'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('title') }),
          },
        ],
        isTranslation: true,
        render: () => (
          <Input />
        ),
      },
      {
        name: 'pushedAt',
        label: t('pushed_at'),
        rules: [
          {
            required: true,
            validator: (rule, value, callback) => {
              if (!value) {
                callback(t('error:please_input', { field: t('pushed_at') }));
              }
              callback();
            },
          },
        ],
        render: () => (
          <DatePicker
            showTime
            disabledDate={(date) => date.isBefore(moment().subtract(1, 'day'))}
          />
        ),
      },
      {
        name: 'type',
        label: t('type'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('type') }),
            type: 'array',
          },
        ],
        render: () => (
          <DataSelector
            allowClear
            mode="multiple"
            dataPath="/v1/notification/content-types"
            defineValue={(option) => option}
            defineLabel={(option) => t(`notification_content_type_${option}`)}
            onChange={(types) => {
              const contentTypes = {};
              for (const type of types) {
                contentTypes[type] = [];
              }
              for (const key of Object.keys(state.contentTypes)) {
                if (types.find((type) => type === key)) {
                  console.log('types.find((type) => type === key):', types.find((type) => type === key));
                  contentTypes[key] = [...state.contentTypes[key]];
                }
              }
              updateState({ contentTypes });
            }}
          />
        ),
      },
      {
        name: 'target',
        label: t('target'),
        rules: [
          {
            required: true,
            validator: (rule, value, callback) => {
              if (!value) {
                callback(t('error:please_input', { field: t('target') }));
              }
              callback();
            },
          },
        ],
        render: () => (
          <NotificationTargetSelector />
        ),
      },
    ];
    return items;
  };

  const convertToFormValue = (data) => {
    if (data) {
      const formValue = {
        ...data,
        pushedAt: moment(data.pushedAt),
        type: Object.keys(data.content),
      };
      return formValue;
    }
    return null;
  };

  const { contentForms } = state;
  return (
    <ListPage
      title={t('notification_management')}
      apiPath="/v1/notification"
      searchInfo={`${t('title')} ${t('translation:or')} ${t('content')}`}
      createBoard={(onCreate, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          }}
          formItems={() => getFormItems()}
          subForms={() => [
            ...contentForms,
          ]}
          onSubmit={(value) => {
            onCreate(value);
          }}
        />
      )}
      onCreateBoardClose={() => updateState({ contentForms: [], contentTypes: {} })}
      createBoardProps={{
        width: 800,
      }}
      updateBoard={(selectedRecord, onUpdate, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          }}
          initialValues={convertToFormValue(selectedRecord)}
          formItems={() => getFormItems()}
          subForms={() => [
            ...contentForms,
          ]}
          onSubmit={(value, changedValue) => {
            onUpdate(selectedRecord.id, changedValue);
          }}
        />
      )}
      onUpdateBoardClose={() => updateState({ contentForms: [], contentTypes: {} })}
      updateBoardProps={{
        width: 800,
      }}
      filter={(onFilterSubmit, onFilterClear, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          }}
          formItems={() => ([
            {
              name: 'updatedAt',
              label: t('updated_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
            {
              name: 'createdAt',
              label: t('created_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
          ])}
          onSubmit={(value: any) => {
            const { updatedAt, createdAt } = value;
            const filterData: FilterData = { ...value };
            if (updatedAt?.length) {
              filterData.updatedAt = {
                from: updatedAt[0],
                to: updatedAt[1],
              };
            } else {
              delete filterData.updatedAt;
            }
            if (createdAt?.length) {
              filterData.createdAt = {
                from: createdAt[0],
                to: createdAt[1],
              };
            } else {
              delete filterData.createdAt;
            }
            onFilterSubmit(filterData);
          }}
          onClear={() => onFilterClear()}
        />
      )}
      columns={(showUpdate, onDelete, loading) => ([
        {
          title: t('notification_id'),
          dataIndex: 'id',
          sorter: true,
        },
        {
          title: t('title'),
          dataIndex: 'title',
          sorter: true,
          render: (title) => title?.[currentLanguage]?.value,
        },
        {
          title: t('system_notification'),
          dataIndex: 'isSystem',
          sorter: true,
          render: (isSystem) => {
            if (isSystem) {
              return t('translation:yes');
            }
            return t('translation:no');
          },
        },
        {
          title: t('pushed_at'),
          dataIndex: 'pushedAt',
          render: (pushedAt) => moment(pushedAt).format('YYYY-MM-DD HH:mm:ss'),
          sorter: true,
        },
        {
          title: t('created_at'),
          dataIndex: 'createdAt',
          render: (createdAt) => moment(createdAt).format('YYYY-MM-DD HH:mm:ss'),
          sorter: true,
        },
        {
          title: t('action'),
          render: (record) => (
            <Row gutter={10}>
              <Col>
                <Button
                  onClick={() => {
                    const contentTypes = {};
                    for (const type of Object.keys(record.content)) {
                      contentTypes[type] = [];
                    }
                    showUpdate(record);
                    updateState({ contentTypes });
                  }}
                >
                  <EditOutlined />
                </Button>
              </Col>
              <Col>
                <Popconfirm
                  title={t('translation:delete_record_confirm_msg')}
                  onConfirm={() => onDelete(record.id)}
                  okText={t('translation:yes')}
                  cancelText={t('translation:no')}
                >
                  <Button loading={loading}>
                    <DeleteOutlined />
                  </Button>
                </Popconfirm>
              </Col>
            </Row>
          ),
        },
      ])}
    />
  );
};

export default NotificationManagement;
