import { useState } from 'react';
import moment from 'moment';
import { Button, DatePicker, Input, Popconfirm, Row, Col } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import ListPage from '@components/list-page';
import BaseForm, { FormItem } from '@components/base-form';
import DataSelector from '@components/data-selector';
import useCommon from '@utils/hooks/common';
import { DateRange } from '@interface';
import styles from './styles';

const { TextArea } = Input;
const { RangePicker } = DatePicker;

interface FilterData {
  updatedAt?: DateRange;
  createdAt?: DateRange;
}

const QueryManagement = () => {
  const { t, language } = useCommon({
    withTranslation: ['query-management'],
    withStyles: styles,
    observers: ['language'],
  });
  const { currentLanguage } = language;
  const [state, setState] = useState({
    variable: [],
  });
  const updateState = (updatedData) => {
    setState((prevState) => ({ ...prevState, ...updatedData }));
  };

  const getVariable = (content) => content.match(/(?<=:)[\w]+/igm) || [];

  const getFormItems = () => {
    const { variable } = state;
    const items: FormItem[] = [
      {
        name: 'type',
        label: t('type'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('type') }),
          },
        ],
        render: () => (
          <DataSelector
            allowClear
            dataPath="/v1/query/type"
            defineValue={(option) => option}
            defineLabel={(option) => option}
          />
        ),
      },
      {
        name: 'title',
        label: t('title'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('title') }),
          },
        ],
        isTranslation: true,
        render: () => (
          <Input />
        ),
      },
      {
        name: 'content',
        label: t('content'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('content') }),
          },
        ],
        render: () => (
          <TextArea
            style={{ fontFamily: 'monospace' }}
            autoSize={{ maxRows: 5, minRows: 3 }}
            onChange={(e) => {
              const { value } = e.target;
              updateState({ variable: getVariable(value) });
            }}
          />
        ),
      },
    ];
    if (variable?.length) {
      for (const key of variable) {
        items.push({
          name: `variable.${key}`,
          label: t('variable_with_key', { key }),
          rules: [
            {
              required: true,
              message: t('error:please_input', { field: t('variable_with_key', { key }) }),
            },
          ],
          render: () => (
            <DataSelector
              dataPath="/v1/form/input/type"
              defineValue={(option) => option}
              defineLabel={(option) => option}
            />
          ),
        });
      }
    }
    return items;
  };
  return (
    <ListPage
      title={t('query_management')}
      apiPath="/v1/query"
      searchInfo={`${t('type')}, ${t('title')} ${t('translation:or')} ${t('content')}`}
      createBoard={(onCreate, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          }}
          formItems={() => getFormItems()}
          onSubmit={(value) => {
            onCreate(value);
          }}
        />
      )}
      createBoardProps={{ width: 700 }}
      onCreateBoardClose={() => updateState({ variable: [] })}
      updateBoard={(selectedRecord, onUpdate, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          }}
          initialValues={selectedRecord}
          formItems={() => getFormItems()}
          onSubmit={(value, changedValue) => {
            onUpdate(selectedRecord.id, changedValue);
          }}
        />
      )}
      updateBoardProps={{ width: 700 }}
      onUpdateBoardClose={() => updateState({ variable: [] })}
      filter={(onFilterSubmit, onFilterClear, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          }}
          formItems={() => ([
            {
              // dataIndex: 'updatedAt',
              name: 'updatedAt',
              label: t('updated_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
            {
              // dataIndex: 'createdAt',
              name: 'createdAt',
              label: t('created_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
          ])}
          onSubmit={(value: any) => {
            const { updatedAt, createdAt } = value;
            const filterData: FilterData = { ...value };
            if (updatedAt?.length) {
              filterData.updatedAt = {
                from: updatedAt[0],
                to: updatedAt[1],
              };
            } else {
              delete filterData.updatedAt;
            }
            if (createdAt?.length) {
              filterData.createdAt = {
                from: createdAt[0],
                to: createdAt[1],
              };
            } else {
              delete filterData.createdAt;
            }
            onFilterSubmit(filterData);
          }}
          onClear={() => onFilterClear()}
        />
      )}
      columns={(showUpdate, onDelete, loading) => ([
        {
          title: t('query_id'),
          dataIndex: 'id',
          sorter: true,
        },
        {
          title: t('type'),
          dataIndex: 'type',
          sorter: true,
        },
        {
          title: t('title'),
          dataIndex: 'title',
          sorter: true,
          render: (title) => title?.[currentLanguage]?.value,
        },
        {
          title: t('updated_at'),
          dataIndex: 'updatedAt',
          render: (updatedAt) => moment(updatedAt).format('YYYY-MM-DD HH:mm:ss'),
          sorter: true,
        },
        {
          title: t('created_at'),
          dataIndex: 'createdAt',
          render: (createdAt) => moment(createdAt).format('YYYY-MM-DD HH:mm:ss'),
          sorter: true,
        },
        {
          title: t('action'),
          render: (record) => (
            <Row gutter={10}>
              <Col>
                <Button
                  onClick={() => {
                    showUpdate(record);
                    updateState({ variable: getVariable(record.content) });
                  }}
                >
                  <EditOutlined />
                </Button>
              </Col>
              <Col>
                <Popconfirm
                  title={t('translation:delete_record_confirm_msg')}
                  onConfirm={() => onDelete(record.id)}
                  okText={t('translation:yes')}
                  cancelText={t('translation:no')}
                >
                  <Button loading={loading}>
                    <DeleteOutlined />
                  </Button>
                </Popconfirm>
              </Col>
            </Row>
          ),
        },
      ])}
    />
  );
};

export default QueryManagement;
