import { useEffect, useState } from 'react';
import { Button, DatePicker, Input, Popconfirm, Select, Switch, Spin, Row, Col } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import ListPage from '@components/list-page';
import BaseForm, { FormItem } from '@components/base-form';
import EditableTable from '@components/editable-table';
import PermissionGroupSelector from '@components/permission-group-selector';
import permissionApi from '@utils/api/permission.api';
import useCommon from '@utils/hooks/common';
import { DateRange } from '@interface';
import styles from './styles';

const { RangePicker } = DatePicker;
const { Option } = Select;

interface FilterData {
  active?: boolean;
  updatedAt?: DateRange;
  createdAt?: DateRange;
}

const ResourceManagement = () => {
  const { t, language } = useCommon({
    withTranslation: ['resource-management'],
    withStyles: styles,
    observers: ['language'],
  });
  const { currentLanguage } = language;
  const [state, setState] = useState({
    allPermissions: null,
  });
  const updateState = (updatedData) => {
    setState((prevState) => ({ ...prevState, ...updatedData }));
  };

  useEffect(() => {
    const getAllPermissions = async () => {
      const response = await permissionApi.getPermissions();
      if (response) {
        const allPermissions = {};
        for (const permission of response) {
          const { id, title } = permission;
          allPermissions[id] = title?.[currentLanguage]?.value || id;
        }
        updateState({
          allPermissions,
        });
      }
    };
    getAllPermissions();
  }, []);

  const getFormItems = (allPermissions) => {
    const formItems: FormItem[] = [
      {
        name: 'id',
        label: t('active'),
        valuePropName: 'checked',
        render: () => (
          <Switch />
        ),
      },
      {
        name: 'permissions',
        valuePropName: 'dataSource',
        render: () => (
          <EditableTable
            columns={[
              {
                title: t('permission_group'),
                dataIndex: 'permissionGroup',
                editable: true,
                rules: [
                  {
                    required: true,
                    message: t('error:please_input', { field: t('permission_group') }),
                  },
                ],
                render: (permissionGroup) => {
                  const arrayValue = [];
                  if (permissionGroup) {
                    const permissionIds = Object.keys(permissionGroup);
                    for (let i = 0; i < permissionIds.length; i += 1) {
                      const permissionDefinition = allPermissions[permissionIds[i]];
                      if (permissionDefinition) {
                        arrayValue.push(`${permissionDefinition}(${permissionGroup[permissionIds[i]]})`);
                      }
                    }
                  }
                  return <span>{arrayValue.join(', ')}</span>;
                },
                editor: () => (
                  <PermissionGroupSelector />
                ),
              },
            ]}
          />
        ),
      },
    ];
    return formItems;
  };

  const { allPermissions } = state;
  if (!allPermissions) {
    return <Spin />;
  }
  return (
    <ListPage
      title={t('resource_management')}
      apiPath="/v1/resource"
      searchInfo={`${t('resource_id')}, ${t('path')}, ${t('method')} ${t('translation:or')} ${t('type')}`}
      defaultState={{ sort: [{ orderBy: 'id', direction: 'ASC' }] }}
      exportEnable
      updateBoard={(selectedRecord, onUpdate, loading) => (
        <BaseForm
          loading={loading}
          formItems={() => getFormItems(allPermissions)}
          initialValues={{
            ...selectedRecord,
            permissions: selectedRecord?.permissions?.map((permission) => ({
              permissionGroup: permission,
            })),
          }}
          onSubmit={(value, changedValue) => {
            const { id } = selectedRecord;
            const { permissions } = changedValue;
            onUpdate(id, { ...changedValue, permissions: permissions.map((permission) => permission.permissionGroup) });
          }}
          formProps={{
            labelCol: { span: 2 },
            wrapperCol: { span: 22 },
          }}
        />
      )}
      updateBoardProps={{ width: 1000 }}
      filter={(onFilterSubmit, onFilterClear, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 8 },
            wrapperCol: { span: 16 },
          }}
          formItems={() => ([
            {
              name: 'path',
              label: t('path'),
              render: () => (
                <Input />
              ),
            },
            {
              name: 'method',
              label: t('method'),
              render: () => (
                <Input />
              ),
            },
            {
              name: 'type',
              label: t('type'),
              render: () => (
                <Select allowClear>
                  <Option value="api">{t('api')}</Option>
                  <Option value="file">{t('file')}</Option>
                </Select>
              ),
            },
            {
              name: 'active',
              label: t('active'),
              render: () => (
                <Select allowClear>
                  <Option value={1}>{t('translation:true')}</Option>
                  <Option value={0}>{t('translation:false')}</Option>
                </Select>
              ),
            },
            {
              name: 'updatedAt',
              label: t('updated_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
            {
              name: 'createdAt',
              label: t('created_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
          ])}
          onSubmit={(value: any) => {
            const { updatedAt, createdAt, active } = value;
            const filterData: FilterData = { ...value };
            if (updatedAt?.length) {
              filterData.updatedAt = {
                from: updatedAt[0],
                to: updatedAt[1],
              };
            } else {
              delete filterData.updatedAt;
            }
            if (createdAt?.length) {
              filterData.createdAt = {
                from: createdAt[0],
                to: createdAt[1],
              };
            } else {
              delete filterData.createdAt;
            }
            if (active !== undefined) {
              filterData.active = !!active;
            }
            onFilterSubmit(filterData);
          }}
          onClear={() => onFilterClear()}
        />
      )}
      columns={(showUpdate, onDelete, loading) => ([
        {
          title: t('resource_id'),
          dataIndex: 'id',
          sorter: true,
        },
        {
          title: t('path'),
          dataIndex: 'path',
          sorter: true,
        },
        {
          title: t('method'),
          dataIndex: 'method',
          sorter: true,
        },
        {
          title: t('type'),
          dataIndex: 'type',
          sorter: true,
        },
        {
          title: t('permissions'),
          dataIndex: 'permissions',
          render: (permissions) => {
            let resourcePermissions = '';
            if (permissions) {
              for (let i = 0; i < permissions.length; i += 1) {
                if (permissions.length > 1) {
                  resourcePermissions += '(';
                }
                const permissionIds = Object.keys(permissions[i]);
                for (const permissionId of permissionIds) {
                  const permissionDefinition = allPermissions[permissionId];
                  if (permissionDefinition) {
                    resourcePermissions += `${permissionDefinition}: ${permissions[i][permissionId]}`;
                    if (permissionId !== permissionIds[permissionIds.length - 1]) {
                      resourcePermissions += ' & ';
                    }
                  }
                }
                if (permissions.length > 1) {
                  resourcePermissions += ')';
                }
                if (i !== permissions.length - 1) {
                  resourcePermissions += ' || ';
                }
              }
            }
            return (
              <span>{resourcePermissions}</span>
            );
          },
        },
        {
          title: t('active'),
          dataIndex: 'active',
          sorter: true,
          render: (active) => (
            <span>{t(active)}</span>
          ),
        },
        {
          title: t('action'),
          render: (record) => (
            <Row gutter={10}>
              <Col>
                <Button
                  onClick={() => showUpdate(record)}
                >
                  <EditOutlined />
                </Button>
              </Col>
              <Col>
                {!record.active && (
                  <Popconfirm
                    title={t('translation:delete_record_confirm_msg')}
                    onConfirm={() => onDelete(record.id)}
                    okText={t('translation:yes')}
                    cancelText={t('translation:no')}
                  >
                    <Button loading={loading}>
                      <DeleteOutlined />
                    </Button>
                  </Popconfirm>
                )}
              </Col>
            </Row>
          ),
        },
      ])}
    />
  );
};

export default ResourceManagement;
