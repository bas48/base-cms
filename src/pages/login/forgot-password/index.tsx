import { useState, useEffect } from 'react';
import { Tabs, Form, Button, Row, Card, Input, Col } from 'antd';
import useCommon from '@utils/hooks/common';
import EmailVerification from '@components/verification/email-verification';
import PhoneVerification from '@components/verification/phone-verification';
import userApi from '@utils/api/user.api';
import useRequest from '@utils/hooks/request';
import Router from 'next/router';
import styles from './styles';

const { TabPane } = Tabs;

const ForgotPassword = () => {
  const { t, classes } = useCommon({
    withTranslation: ['forgot-password'],
    withStyles: styles,
  });
  const [form] = Form.useForm();
  const [activeTab, setActiveTab] = useState('email');
  const [token, setToken] = useState(null);
  const [showReset, setShowReset] = useState(false);

  const { run: resetPassword, loading: resetLoading } = useRequest(async (formData) => {
    const { password } = formData;
    await userApi.resetPassword(token, password);
    Router.push('/login');
  }, { showError: true, successMessage: t('translation:success') });

  useEffect(() => {
    if (token) {
      setShowReset(true);
    }
  }, [token]);

  return (
    <div className={classes.main}>
      <Card
        title={t((showReset && 'reset_password') || 'forgot_password')}
        className={classes.forgotPassword}
      >
        <Form
          form={form}
          wrapperCol={{ span: 24 }}
          onFinish={resetPassword}
        >
          {
            (showReset && (
              <>
                <Form.Item
                  name="password"
                  label={t('password')}
                  rules={[
                    {
                      required: true,
                      message: t('error:please_confirm', { field: t('password') }),
                    },
                    {
                      validator: (rule, value, callback) => {
                        if (value) {
                          form.validateFields(['confirmPassword']);
                        }
                        callback();
                      },
                    },
                  ]}
                >
                  <Input.Password />
                </Form.Item>
                <Form.Item
                  name="confirmPassword"
                  label={t('confirm_password')}
                  rules={[
                    {
                      required: true,
                      message: t('error:please_confirm', { field: t('confirm_password') }),
                    },
                    {
                      validator: (rule, value, callback) => {
                        if (value && value !== form.getFieldValue('password')) {
                          callback(t('error:password_inconsistent'));
                        } else if (form.getFieldValue('password') && !value && !true) {
                          callback(t('error:please_confirm', { field: t('password') }));
                        } else {
                          callback();
                        }
                      },
                    },
                  ]}
                >
                  <Input.Password />
                </Form.Item>
              </>
            )) || (
              <Tabs
                defaultActiveKey={activeTab}
                onChange={(tab) => setActiveTab(tab)}
              >
                <TabPane tab={t('email')} key="email">
                  <EmailVerification
                    form={form}
                    isGuest={false}
                    onToken={(value) => setToken(value)}
                  />
                </TabPane>
                <TabPane tab={t('phone')} key="phone">
                  <PhoneVerification
                    form={form}
                    isGuest={false}
                    onToken={(value) => setToken(value)}
                  />
                </TabPane>
              </Tabs>

            )
          }
          <Row justify="center" gutter={10}>
            {
              showReset && (
                <Col>
                  <Form.Item>
                    <Button type="primary" htmlType="submit" loading={resetLoading}>
                      {t('reset_password')}
                    </Button>
                  </Form.Item>
                </Col>
              )
            }
            <Col>
              <Button onClick={() => Router.push('/login')}>
                {t('cancel')}
              </Button>
            </Col>
          </Row>
        </Form>
      </Card>
    </div>
  );
};

export default ForgotPassword;
