const styles = {
  main: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 100,
  },
  resetPassword: {
    width: 500,
  },
  forgotPassword: {
    width: 500,
  },
};

export default styles;
