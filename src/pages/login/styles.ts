const styles = {
  login: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '150px 0',
  },
  projectTitle: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: '1.5em',
    marginBottom: '0.3em',
  },
  card: {
    width: 'fit-content',
    minWidth: 500,
  },
  socialLogin: {
    marginTop: 20,
    flexDirection: 'column',
    '& .ant-card-body': {
      display: 'flex',
      justifyContent: 'space-around',
      alignContent: 'center',
    },
  },
};

export default styles;
