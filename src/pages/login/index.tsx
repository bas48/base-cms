import { Button, Card, Input, Form, Col, Row } from 'antd';
import Router from 'next/router';
import getConfig from 'next/config';
import { UserOutlined, KeyOutlined } from '@ant-design/icons';
import useCommon from '@utils/hooks/common';
import useRequest from '@utils/hooks/request';
import styles from './styles';

const { REACT_PROJECT_NAME } = getConfig().publicRuntimeConfig;

const Login = () => {
  const { t, classes, user } = useCommon({
    withStyles: styles,
    withTranslation: ['login'],
    observers: ['user'],
  });
  const { run: userLogin, loading: loginLoading } = useRequest(async (formData) => {
    const { userId, password } = formData;
    await user.login(userId, password);
    Router.push('/');
  }, { showError: true });
  return (
    <div className={classes.login}>
      <div>
        <div className={classes.projectTitle}>
          {REACT_PROJECT_NAME}
        </div>
        <Card title={t('login')} className={classes.card}>
          <Form
            wrapperCol={{ span: 24 }}
            onFinish={userLogin}
          >
            <Form.Item
              name="userId"
              rules={[{
                required: true,
                message: t('error:please_input', { field: t('user_id') }),
              }]}
            >
              <Input
                prefix={<UserOutlined />}
                placeholder={t('user_id')}
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[{
                required: true,
                message: (t('error:please_input', { field: t('password') })),
              }]}
            >
              <Input.Password
                prefix={<KeyOutlined />}
                placeholder={t('password')}
                autoComplete="on"
              />
            </Form.Item>
            <Row gutter={10}>
              <Col>
                <Form.Item>
                  <Button type="primary" htmlType="submit" loading={loginLoading}>
                    {t('login')}
                  </Button>
                </Form.Item>
              </Col>
              <Col>
                <Button type="link" onClick={() => Router.push('/login/forgot-password')} loading={loginLoading}>
                  {t('forgot_password')}
                </Button>
              </Col>
            </Row>
          </Form>
        </Card>
      </div>
    </div>
  );
};
export default Login;
