import moment from 'moment';
import { Button, DatePicker, Input, Popconfirm, Row, Col } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import ListPage from '@components/list-page';
import BaseForm, { FormItem } from '@components/base-form';
import useCommon from '@utils/hooks/common';
import { DateRange } from '@interface';
import styles from './styles';

const { RangePicker } = DatePicker;

interface FilterData {
  updatedAt?: DateRange;
  createdAt?: DateRange;
}

const PermissionManagement = () => {
  const { t, language } = useCommon({
    withTranslation: ['permission-management'],
    withStyles: styles,
    observers: ['language'],
  });
  const { currentLanguage } = language;

  const getFormItems = (form, isCreate) => {
    const items: FormItem[] = isCreate ? [
      {
        name: 'id',
        label: t('permission_id'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('permission_id') }),
          },
        ],
        render: () => (
          <Input />
        ),
      },
    ] : [];
    items.push({
      name: 'title',
      label: t('title'),
      rules: [
        {
          required: true,
          message: t('error:please_input', { field: t('title') }),
        },
      ],
      isTranslation: true,
      render: () => (
        <Input />
      ),
    });
    return items;
  };

  return (
    <ListPage
      title={t('permission_management')}
      apiPath="/v1/permission"
      searchInfo={t('permission_id')}
      defaultState={{ sort: [{ orderBy: 'id', direction: 'ASC' }] }}
      createBoard={(onCreate, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 8 },
            wrapperCol: { span: 16 },
          }}
          formItems={(form) => getFormItems(form, true)}
          onSubmit={(value) => {
            onCreate(value);
          }}
        />
      )}
      updateBoard={(selectedRecord, onUpdate, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 8 },
            wrapperCol: { span: 16 },
          }}
          initialValues={selectedRecord}
          formItems={(form) => getFormItems(form, false)}
          onSubmit={(value, changedValue) => {
            onUpdate(selectedRecord.id, changedValue);
          }}
        />
      )}
      filter={(onFilterSubmit, onFilterClear, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 8 },
            wrapperCol: { span: 16 },
          }}
          formItems={() => ([
            {
              name: 'updatedAt',
              label: t('updated_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
            {
              name: 'createdAt',
              label: t('created_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
          ])}
          onSubmit={(value: any) => {
            const { updatedAt, createdAt } = value;
            const filterData: FilterData = { ...value };
            if (updatedAt?.length) {
              filterData.updatedAt = {
                from: updatedAt[0],
                to: updatedAt[1],
              };
            } else {
              delete filterData.updatedAt;
            }
            if (createdAt?.length) {
              filterData.createdAt = {
                from: createdAt[0],
                to: createdAt[1],
              };
            } else {
              delete filterData.createdAt;
            }
            onFilterSubmit(filterData);
          }}
          onClear={() => onFilterClear()}
        />
      )}
      columns={(showUpdate, onDelete, loading) => ([
        {
          title: t('permission_id'),
          dataIndex: 'id',
          sorter: true,
        },
        {
          title: t('title'),
          dataIndex: 'title',
          sorter: true,
          render: (title) => title?.[currentLanguage]?.value,
        },
        {
          title: t('updated_at'),
          dataIndex: 'updatedAt',
          render: (updatedAt) => moment(updatedAt).format('YYYY-MM-DD HH:mm:ss'),
          sorter: true,
        },
        {
          title: t('created_at'),
          dataIndex: 'createdAt',
          render: (createdAt) => moment(createdAt).format('YYYY-MM-DD HH:mm:ss'),
          sorter: true,
        },
        {
          title: t('action'),
          render: (record) => (
            <Row gutter={10}>
              <Col>
                <Button
                  onClick={() => showUpdate(record)}
                >
                  <EditOutlined />
                </Button>
              </Col>
              <Col>
                <Popconfirm
                  title={t('translation:delete_record_confirm_msg')}
                  onConfirm={() => onDelete(record.id)}
                  okText={t('translation:yes')}
                  cancelText={t('translation:no')}
                >
                  <Button loading={loading}>
                    <DeleteOutlined />
                  </Button>
                </Popconfirm>
              </Col>
            </Row>
          ),
        },
      ])}
    />
  );
};

export default PermissionManagement;
