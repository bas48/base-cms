import { useState } from 'react';
import moment from 'moment';
import { Button, Card, DatePicker, Input, Modal, Popconfirm, Switch, Row, Col } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import permissionApi from '@utils/api/permission.api';
import roleApi from '@utils/api/role.api';
import ListPage from '@components/list-page';
import BaseForm, { FormItem } from '@components/base-form';
import PermissionAssigner from '@components/permission-assigner';
import PhoneInput from '@components/verification/phone-input';
import FileUpload from '@components/file-upload';
import useCommon from '@utils/hooks/common';
import useRequest from '@utils/hooks/request';
import DataSelector from '@components/data-selector';
import { DateRange } from '@interface';
import styles from './styles';

const { RangePicker } = DatePicker;

interface FilterData {
  updatedAt?: DateRange;
  createdAt?: DateRange;
}

const UserManagement = () => {
  const { t, user: userStore, language } = useCommon({
    withTranslation: ['user-management'],
    withStyles: styles,
    observers: ['user', 'language'],
  });
  const { currentLanguage } = language;
  const [state, setState] = useState({
    selectedUser: null,
    selectedPermissions: null,
    showPermissionAssign: false,
  });
  const updateState = (updatedData) => {
    setState((prevState) => ({ ...prevState, ...updatedData }));
  };

  const getFormItems = (form, isCreate) => {
    const formItems: FormItem[] = [
      {
        name: 'roles',
        label: t('role'),
        render: () => (
          <DataSelector
            mode="multiple"
            dataPath="/v1/role"
            defineValue={(option) => option.id}
            defineLabel={(option) => option?.title?.[currentLanguage]?.value || option.id}
          />
        ),
      },
      {
        name: 'name',
        label: t('name'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('name') }),
          },
        ],
        render: () => (
          <Input />
        ),
      },
      {
        name: 'icon',
        label: t('icon'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('icon') }),
          },
        ],
        render: () => (
          <FileUpload sizeLimit={1} limit={1} accept="image/*" />
        ),
      },
      {
        name: 'email',
        label: t('email'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('email') }),
          },
          {
            type: 'email',
            message: t('error:please_input_correct_type_format', { type: t('email') }),
          },
        ],
        render: () => (
          <Input />
        ),
      },
      {
        name: 'phone',
        label: t('phone'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('phone') }),
          },
        ],
        valuePropName: 'defaultValue',
        render: () => (
          <PhoneInput />
        ),
      },
      {
        name: 'banned',
        label: t('banned'),
        valuePropName: 'checked',
        render: () => (
          <Switch />
        ),
      },
      {
        name: 'password',
        label: t('password'),
        rules: [
          {
            required: isCreate,
            message: t('error:please_confirm', { field: t('password') }),
          },
          {
            validator: (rule, value, callback) => {
              if (value) {
                form.validateFields(['confirmPassword'], { force: true });
              }
              callback();
            },
          },
        ],
        render: () => (
          <Input.Password />
        ),
      },
      {
        name: 'confirmPassword',
        label: t('confirm_password'),
        rules: [
          {
            required: isCreate,
            message: t('error:please_confirm', { field: t('confirm_password') }),
          },
          {
            validator: (rule, value, callback) => {
              if (value && value !== form.getFieldValue('password')) {
                callback(t('error:password_inconsistent'));
              } else if (form.getFieldValue('password') && !value && !isCreate) {
                callback(t('error:please_confirm', { field: t('password') }));
              } else {
                callback();
              }
            },
          },
        ],
        render: () => (
          <Input.Password />
        ),
      },
    ];
    return formItems;
  };

  const { run: onShowPermissionAssign } = useRequest(async (record) => {
    const userPermissions = await permissionApi.getUserPermissions(record.id);
    const selectedPermissions = {};
    for (const userPermission of userPermissions) {
      const { permissionId, right } = userPermission;
      selectedPermissions[permissionId] = right;
    }
    updateState({ showPermissionAssign: true, selectedPermissions, selectedUser: record });
  }, { showError: true });

  const { run: onAssignPermission, loading: assignPermissionLoading } = useRequest(async (selectedUser, value) => {
    const permissions = [];
    for (const permissionId of Object.keys(value)) {
      if (value[permissionId]) {
        permissions.push({
          permissionId,
          right: value[permissionId],
        });
      }
    }
    await permissionApi.assignUserPermissions(selectedUser.id, permissions);
    updateState({
      showPermissionAssign: false,
      selectedPermissions: null,
      selectedUser: null,
    });
  }, { showError: true, successMessage: t('translation:success') });

  const { selectedUser, showPermissionAssign, selectedPermissions } = state;
  return (
    <ListPage
      title={t('user_management')}
      apiPath="/v1/user"
      searchInfo={t('user_id')}
      defaultState={{ sort: [{ orderBy: 'id', direction: 'ASC' }] }}
      createBoard={(onCreate, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 8 },
            wrapperCol: { span: 16 },
          }}
          formItems={(form) => getFormItems(form, true)}
          onSubmit={(value) => {
            const data = { ...value };
            delete data.confirmPassword;
            let roles;
            if (data.roles?.length) {
              roles = [...data.roles];
            }
            delete data.roles;
            onCreate(data, async (user) => {
              if (roles?.length) {
                await roleApi.assignUserRoles(user.id, roles);
              }
            });
          }}
        />
      )}
      updateBoard={(selectedRecord, onUpdate, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 8 },
            wrapperCol: { span: 16 },
          }}
          initialValues={{
            ...selectedRecord,
            roles: selectedRecord?.roles.map((role) => role.id),
          }}
          formItems={(form) => getFormItems(form, false)}
          onSubmit={(value, changedValue) => {
            const data = { ...changedValue };
            delete data.confirmPassword;
            let roles;
            // eslint-disable-next-line no-prototype-builtins
            if (data.hasOwnProperty('roles')) {
              if (data.roles === undefined) {
                roles = [];
              } else {
                roles = [...data.roles];
              }
              delete data.roles;
            }
            onUpdate(selectedRecord.id, data, async (user) => {
              if (roles) {
                await roleApi.assignUserRoles(user.id, roles);
              }
              if (user.id === userStore.info.id) {
                userStore.getPersonalInfo();
              }
            });
          }}
        />
      )}
      filter={(onFilterSubmit, onFilterClear, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          }}
          formItems={() => ([
            {
              name: 'roleId',
              label: t('role'),
              render: () => (
                <DataSelector
                  allowClear
                  dataPath="/v1/role"
                  defineValue={(option) => option.id}
                  defineLabel={(option) => option?.title?.[currentLanguage]?.value || option.id}
                />
              ),
            },
            {
              name: 'updatedAt',
              label: t('updated_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
            {
              name: 'createdAt',
              label: t('created_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
          ])}
          onSubmit={(value: any) => {
            const { updatedAt, createdAt } = value;
            const filterData: FilterData = { ...value };
            if (updatedAt?.length) {
              filterData.updatedAt = {
                from: updatedAt[0],
                to: updatedAt[1],
              };
            } else {
              delete filterData.updatedAt;
            }
            if (createdAt?.length) {
              filterData.createdAt = {
                from: createdAt[0],
                to: createdAt[1],
              };
            } else {
              delete filterData.createdAt;
            }
            onFilterSubmit(filterData);
          }}
          onClear={() => onFilterClear()}
        />
      )}
      columns={(showUpdate, onDelete, loading) => (
        [
          {
            title: t('role'),
            dataIndex: 'roles',
            sorter: true,
            render: (roles) => (
              <span>{roles.map((role) => role.title).toString()}</span>
            ),
          },
          {
            title: t('user_id'),
            dataIndex: 'id',
            sorter: true,
          },
          {
            title: t('name'),
            dataIndex: 'name',
            sorter: true,
          },
          {
            title: t('email'),
            dataIndex: 'email',
            sorter: true,
          },
          {
            title: t('phone'),
            dataIndex: 'phone',
            sorter: true,
          },
          {
            title: t('banned'),
            dataIndex: 'banned',
            sorter: true,
            render: (banned) => t(`${banned}`),
          },
          {
            title: t('language'),
            dataIndex: 'language',
            sorter: true,
          },
          {
            title: t('created_at'),
            dataIndex: 'createdAt',
            render: (createdAt) => moment(createdAt).format('YYYY-MM-DD HH:mm:ss'),
            sorter: true,
          },
          {
            title: t('action'),
            render: (record) => (
              <Row gutter={10}>
                <Col>
                  <Button
                    onClick={async () => showUpdate(record)}
                  >
                    <EditOutlined />
                  </Button>
                </Col>
                <Col>
                  <Popconfirm
                    title={t('translation:delete_record_confirm_msg')}
                    onConfirm={() => onDelete(record.id)}
                    okText={t('translation:yes')}
                    cancelText={t('translation:no')}
                  >
                    <Button loading={loading}>
                      <DeleteOutlined />
                    </Button>
                  </Popconfirm>
                </Col>
                <Col>
                  <Button
                    onClick={() => onShowPermissionAssign(record)}
                  >
                    {t('assign_permission')}
                  </Button>
                </Col>
              </Row>
            ),
          },
        ]
      )}
    >
      <Modal
        bodyStyle={{ padding: 0 }}
        visible={showPermissionAssign}
        footer={null}
        maskClosable={false}
        destroyOnClose
        onCancel={() => updateState({
          showPermissionAssign: false,
          selectedPermissions: null,
          selectedUser: null,
        })}
      >
        <Card title={t('permission_assigner')}>
          <PermissionAssigner
            supportBanned
            loading={assignPermissionLoading}
            initialValues={selectedPermissions}
            onSubmit={(value) => onAssignPermission(selectedUser, value)}
          />
        </Card>
      </Modal>
    </ListPage>
  );
};

export default UserManagement;
