/* eslint-disable no-case-declarations */
import { useState } from 'react';
import { Button, Card, Input, Select, Col, Row, Spin } from 'antd';
import SocketClient from '@utils/socket';
import classnames from 'classnames';
import BaseForm from '@components/base-form';
import { SOCKET_STATUS } from '@constant/socket';
import useCommon from '@utils/hooks/common';
import styles from './styles';

const { TextArea } = Input;
const { Option } = Select;

const SocketTest = () => {
  const { t, classes } = useCommon({
    withTranslation: ['socket'],
    withStyles: styles,
  });
  const [state, setState] = useState({
    status: SOCKET_STATUS.disconnected,
    client: null,
    connecting: false,
    response: '',
    unsubscribe: null,
    currentEvent: '',
  });
  const updateState = (updatedData) => {
    setState((prevState) => ({ ...prevState, ...updatedData }));
  };
  const addResponse = (message) => {
    let { response } = state;
    if (!response) {
      response = message;
    } else {
      response += `\n${message}`;
    }
    updateState({ response });
  };
  const initSocket = (path) => {
    const client = new SocketClient(path);
    updateState({
      client,
      status: SOCKET_STATUS.connecting,
    });
    client.on('connect', () => {
      addResponse(t('socket_connected_to', { path: client.path, interpolation: { escapeValue: false } }));
      updateState({ status: SOCKET_STATUS.connected });
    });
    client.on('disconnect', () => {
      addResponse(t('socket_disconnected_with', { path: client.path, interpolation: { escapeValue: false } }));
      updateState({
        status: SOCKET_STATUS.connecting,
      });
    });
    client.on('error', (err) => {
      console.log('Socket Error:', err);
      addResponse(`Socket Error: ${err}`);
    });
  };

  const closeConnection = async () => {
    const { client, unsubscribe } = state;
    if (unsubscribe) {
      await unsubscribe();
    }
    client.close();
    updateState({
      client: null,
      status: SOCKET_STATUS.disconnected,
      response: '',
      unsubscribe: null,
    });
  };

  const onSubmit = async (data) => {
    const { method, event, params } = data;
    const { client } = state;
    try {
      switch (method) {
        case 'call':
          let response = await client.call(event, params ? JSON.parse(params) : null);
          if (typeof response === 'object') {
            response = JSON.stringify(response, undefined, 4);
          }
          if (response) {
            addResponse(t('socket_server_response', { response, interpolation: { escapeValue: false } }));
          }
          break;
        case 'subscribe':
          const unsubscribe = await client.subscribe(event, params ? JSON.parse(params) : null, (res) => {
            addResponse(t('socket_server_response', {
              response: typeof res === 'object' ? JSON.stringify(res, undefined, 4) : res,
              interpolation: { escapeValue: false },
            }));
          });
          updateState({ unsubscribe, currentEvent: event });
          break;
        default:
          break;
      }
    } catch (error) {
      const { message, extras } = error;
      console.log('Error:', message, extras);
      let errorMsg = message;
      if (extras && typeof extras === 'string') {
        errorMsg += `: ${extras}`;
      } else if (typeof extras === 'object') {
        const { errors } = extras;
        if (errors) {
          for (const extraError of errors) {
            if (typeof extraError === 'string') {
              errorMsg += `: ${extraError}`;
            }
          }
        }
      }
      addResponse(t('socket_error', { error: errorMsg, interpolation: { escapeValue: false } }));
    }
  };

  const getContent = () => {
    const { status, client, response, unsubscribe } = state;
    switch (status) {
      case SOCKET_STATUS.disconnected:
        return (
          <BaseForm
            formItems={() => ([
              {
                name: 'path',
                label: t('path'),
                rules: [
                  {
                    required: true,
                    message: t('error:please_input', { field: t('path') }),
                  },
                ],
                render: () => (
                  <Input
                    allowClear
                  />
                ),
              },
            ])}
            customFooter={(reset) => (
              <>
                <Button
                  type="primary"
                  htmlType="submit"
                >
                  {t('connect')}
                </Button>
                <Button
                  onClick={() => {
                    reset();
                  }}
                >
                  {t('clear')}
                </Button>
              </>
            )}
            onSubmit={async (value: any) => {
              const { path } = value;
              initSocket(path);
            }}
          />
        );
      case SOCKET_STATUS.connecting:
        return (
          <Row className={classnames(classes.centralize, classes.connecting)}>
            <Row className={classes.spin}>
              <Spin tip={t('connecting_to', { path: client.path, interpolation: { escapeValue: false } })} />
            </Row>
            <Row>
              <Button
                onClick={() => closeConnection()}
              >
                {t('cancel')}
              </Button>
            </Row>
          </Row>
        );
      case SOCKET_STATUS.connected:
        return (
          <Row className={classes.connected}>
            <Col span={11}>
              {
                unsubscribe
                  ? (
                    <Row className={classes.centralize}>
                      <>{t('socket_subscribe_to', { event: state.currentEvent, interpolation: { escapeValue: false } })}</>
                      <Button
                        style={{ marginLeft: 15 }}
                        onClick={async () => {
                          if (unsubscribe) {
                            const unsubscribeRes = await unsubscribe();
                            addResponse(t('socket_server_response', { unsubscribeRes, interpolation: { escapeValue: false } }));
                            updateState({ unsubscribe: null, currentEvent: '' });
                          }
                        }}
                      >
                        {t('unsubscribe')}
                      </Button>
                    </Row>
                  )
                  : (
                    <BaseForm
                      formItems={() => ([
                        {
                          name: 'method',
                          label: t('method'),
                          rules: [
                            {
                              required: true,
                              message: t('error:please_input', { field: t('method') }),
                            },
                          ],
                          render: () => (
                            <Select>
                              {
                                ['call', 'subscribe'].map((method) => (
                                  <Option key={method} value={method}>{t(method)}</Option>
                                ))
                              }
                            </Select>
                          ),
                        },
                        {
                          name: 'event',
                          label: t('event'),
                          rules: [
                            {
                              required: true,
                              message: t('error:please_input', { field: t('event') }),
                            },
                          ],
                          render: () => (
                            <Input
                              allowClear
                            />
                          ),
                        },
                        {
                          name: 'params',
                          label: t('parameters'),
                          rules: [
                            {
                              validator: (rule, value, callback) => {
                                console.log('value:', value);
                                try {
                                  if (value) {
                                    JSON.parse(value);
                                  }
                                  callback();
                                } catch (error) {
                                  callback(t('error:please_input', { field: t('correct_json_string') }));
                                }
                              },
                            },
                          ],
                          render: () => (
                            <TextArea
                              style={{ fontFamily: 'monospace' }}
                              autoSize={{ minRows: 10, maxRows: 10 }}
                            />
                          ),
                        },
                      ])}
                      onSubmit={async (value: any) => onSubmit(value)}
                    />
                  )
              }
            </Col>
            <Col span={11} className={classes.response}>
              <div className={classnames(classes.centralize, 'title')}>
                <>{t('response')}</>
                <div className={classes.space} />
                <Button onClick={() => updateState({ response: '' })}>{t('clear')}</Button>
              </div>
              <TextArea style={{ fontFamily: 'monospace' }} value={response} autoSize={{ minRows: 20, maxRows: 20 }} />
            </Col>
          </Row>
        );
      default:
        return <div>{t('invalid_status')}</div>;
    }
  };

  const { client, status } = state;
  return (
    <Card
      title={
        status === SOCKET_STATUS.connected
          ? (
            <Row className={classes.connection}>
              <>
                {t('socket_connected_to', { path: client.path, interpolation: { escapeValue: false } })}
              </>
              <Button
                onClick={() => {
                  closeConnection();
                }}
              >
                {t('disconnect')}
              </Button>
            </Row>
          ) : t('socket_test')
      }
    >
      {getContent()}
    </Card>
  );
};

export default SocketTest;
