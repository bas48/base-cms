const styles = {
  connection: {
    display: 'flex',
    justifyContent: 'left',
    alignItems: 'center',
    '& button': {
      marginLeft: 10,
    },
  },
  centralize: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  connecting: {
    flexDirection: 'column',
  },
  connected: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
  },
  spin: {
    marginBottom: 15,
  },
  response: {
    '& .title': {
      height: 39,
    },
    '& textarea': {
      marginTop: 2,
    },
  },
  space: {
    flex: 1,
  },
};

export default styles;
