/* eslint-disable react/jsx-props-no-spreading */
import { useState, useEffect } from 'react';
import useCommon from '@utils/hooks/common';
import App from 'next/app';
import 'antd/dist/antd.less';
import acceptLanguage from 'accept-language';
import { ThemeProvider, JssProvider, SheetsRegistry } from 'react-jss';
import getConfig from 'next/config';
import Head from 'next/head';
import Cookies from 'js-cookie';
import { Spin } from 'antd';
import AntConfigProvider from '@components/ant-config-provider';
import { autorun } from 'mobx';
import equal from 'deep-equal';
import { Provider, Observer } from 'mobx-react';
import initializeStore from '@utils/mobx';
import setLanguage from '@utils/i18n';
import { I18nextProvider } from 'react-i18next';
import languageApi from '@utils/api/language.api';
import translationApi from '@utils/api/translation.api';
import Layout from '@components/layout';
import { getTheme, getCurrentStyleFormat } from '@utils/theme';

const sheets = new SheetsRegistry();

const { publicRuntimeConfig } = getConfig();
const { REACT_WEB_TYPE } = publicRuntimeConfig;

const isMobxReady = (mobxStore) => {
  for (const key of Object.keys(mobxStore)) {
    if (!mobxStore[key].isReady) {
      console.log('not ready:', key);
      return false;
    }
  }
  return true;
};

// eslint-disable-next-line
const MyApp = ({ Component, pageProps, initialMobxState, i18nResource, currentLanguage }) => {
  const isServer = !process.browser;
  const mobxStore = isServer ? initialMobxState : initializeStore(initialMobxState);
  const [i18n, setI18n] = useState(i18nResource ? setLanguage(currentLanguage, i18nResource) : null);
  const [styleFormat, setStyleFormat] = useState(null);

  const { t } = useCommon({
    withTranslation: null,
  });

  useEffect(() => {
    autorun(() => {
      i18n.changeLanguage(mobxStore.language.currentLanguage);
      if (!isServer) {
        Cookies.set('currentLanguage', mobxStore.language.currentLanguage, { expires: 7 });
      }
    });
  }, []);

  useEffect(() => {
    const fetchI18n = async () => {
      if (!i18n) {
        const translationSources = await translationApi.getTranslationSource(REACT_WEB_TYPE);
        setI18n(setLanguage(currentLanguage, translationSources));
      }
    };
    const resizeListener = () => {
      const currentStyleFormat = getCurrentStyleFormat(window.innerWidth);
      if (currentStyleFormat && !equal(currentStyleFormat, styleFormat)) {
        setStyleFormat(currentStyleFormat);
      }
    };
    resizeListener();
    window.addEventListener('resize', resizeListener);
    fetchI18n();
    return () => {
      window.removeEventListener('resize', resizeListener);
    };
  });

  return (
    <>
      <Head>
        <title>{t('theme')}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charSet="utf-8" />
      </Head>
      <Provider {...mobxStore}>
        <I18nextProvider i18n={i18n}>
          <JssProvider registry={sheets}>
            <AntConfigProvider>
              <ThemeProvider theme={getTheme(styleFormat)}>
                <Observer
                  render={() => (
                    styleFormat && isMobxReady(mobxStore)
                      ? (
                        <Layout>
                          <Component {...pageProps} />
                        </Layout>
                      ) : <Spin />
                  )}
                />
              </ThemeProvider>
            </AntConfigProvider>
          </JssProvider>
        </I18nextProvider>
      </Provider>
    </>
  );
};

MyApp.getInitialProps = async (appContext) => {
  const isServer = !process.browser;
  let pageProps = {};
  if (App.getInitialProps) {
    pageProps = await App.getInitialProps(appContext);
  }

  if (isServer) {
    const { ctx } = appContext;
    // Get translation resources
    const i18nResource = await translationApi.getTranslationSource(REACT_WEB_TYPE);

    // Get supported languages from backend
    const languages = await languageApi.getLanguages();

    // Get current language from cookies
    let currentLanguage = ctx.req.ctx.cookies.get('currentLanguage');
    console.log('Cookie Stored Language:', currentLanguage);

    if (!currentLanguage) {
      // Get current language from browser
      const languageIDs = languages.map((language) => language.id);
      console.log('Supported Languages:', languageIDs);
      acceptLanguage.languages(languageIDs);
      if (ctx.req) {
        currentLanguage = acceptLanguage.get(ctx.req.headers['accept-language']);
        console.log('Browser Language:', currentLanguage);
      }
    }
    // Set Support languages and current language to mobx
    const mobxStore = initializeStore({
      language: {
        supportLanguages: languages,
        currentLanguage,
      },
    });
    ctx.mobxStore = mobxStore;
    return { pageProps, initialMobxState: mobxStore, i18nResource, currentLanguage };
  }
  return { ...pageProps };
};

export default MyApp;
