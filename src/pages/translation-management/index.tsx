import { Button, Input } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import ListPage from '@components/list-page';
import DataSelector from '@components/data-selector';
import BaseForm, { FormItem } from '@components/base-form';
import useCommon from '@utils/hooks/common';
import styles from './styles';

const { TextArea } = Input;

const TranslationManagement = () => {
  const { t } = useCommon({
    withTranslation: ['translation-management'],
    withStyles: styles,
  });

  const getFormItems = () => {
    const formItems: FormItem[] = [
      {
        name: 'content',
        label: t('content'),
        isTranslation: true,
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('content') }),
          },
        ],
        render: () => (
          <TextArea rows={4} />
        ),
      },
    ];
    return formItems;
  };

  return (
    <ListPage
      title={t('translation_management')}
      apiPath="/v1/translation"
      searchInfo={t('content')}
      defaultState={{
        sort: [{
          orderBy: 'platform',
          direction: 'ASC',
        }],
      }}
      multiSortEnable={false}
      updateBoard={(selectedRecord, onUpdate, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 8 },
            wrapperCol: { span: 16 },
          }}
          initialValues={selectedRecord}
          formItems={() => getFormItems()}
          onSubmit={(value, changedValue) => {
            const { platform, field } = selectedRecord;
            const { translations: transData } = changedValue;
            const translations = [];
            for (const languageId of Object.keys(transData)) {
              translations.push({
                languageId,
                content: transData[languageId].content,
              });
            }
            onUpdate(`${platform}/${field}`, {
              translations,
            });
          }}
        />
      )}
      filter={(onFilterSubmit, onFilterClear, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 8 },
            wrapperCol: { span: 16 },
          }}
          formItems={() => ([
            {
              // dataIndex: 'platform',
              name: 'platform',
              label: t('platform'),
              render: () => (
                <DataSelector
                  dataPath="/v1/translation/support-platform"
                  defineValue={(option) => option}
                  defineLabel={(option) => option}
                />
              ),
            },
            {
              // dataIndex: 'field',
              name: 'field',
              label: t('field'),
              render: () => (
                <Input />
              ),
            },
          ])}
          onSubmit={(value: any) => onFilterSubmit(value)}
          onClear={() => onFilterClear()}
        />
      )}
      columns={(showUpdate) => ([
        {
          title: t('platform'),
          dataIndex: 'platform',
          sorter: true,
        },
        {
          title: t('field'),
          dataIndex: 'field',
          sorter: true,
        },
        {
          title: t('action'),
          render: (record) => (
            <Button
              onClick={() => showUpdate(record)}
            >
              <EditOutlined />
            </Button>
          ),
        },
      ])}
    />
  );
};

export default TranslationManagement;
