import { useState } from 'react';
import moment from 'moment';
import { Button, Card, Row, Col, DatePicker, Input, Modal, Popconfirm } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import permissionApi from '@utils/api/permission.api';
import ListPage from '@components/list-page';
import BaseForm, { FormItem } from '@components/base-form';
import PermissionAssigner from '@components/permission-assigner';
import useCommon from '@utils/hooks/common';
import useRequest from '@utils/hooks/request';
import { DateRange } from '@interface';
import styles from './styles';

const { RangePicker } = DatePicker;

interface FilterData {
  updatedAt?: DateRange;
  createdAt?: DateRange;
}

const RoleManagement = () => {
  const { t, language, permission } = useCommon({
    withTranslation: ['role-management'],
    withStyles: styles,
    observers: ['language', 'permission'],
  });
  const { currentLanguage } = language;
  const [state, setState] = useState({
    selectedRole: null,
    selectedPermissions: null,
    showPermissionAssign: false,
  });
  const updateState = (updatedData) => {
    setState((prevState) => ({ ...prevState, ...updatedData }));
  };

  const getFormItems = () => {
    const formItems: FormItem[] = [
      {
        name: 'title',
        label: t('title'),
        rules: [
          {
            required: true,
            message: t('error:please_input', { field: t('title') }),
          },
        ],
        isTranslation: true,
        render: () => (
          <Input />
        ),
      },
    ];
    return formItems;
  };

  const { run: onShowPermissionAssign } = useRequest(async (record) => {
    const rolePermissions = await permissionApi.getRolePermissions(record.id);
    updateState({ showPermissionAssign: true, selectedPermissions: rolePermissions, selectedRole: record });
  }, { showError: true });

  const { run: onAssignPermission, loading: assignPermissionLoading } = useRequest(async (selectedRole, value) => {
    const permissions = [];
    for (const permissionId of Object.keys(value)) {
      if (value[permissionId]) {
        permissions.push({
          permissionId,
          right: value[permissionId],
        });
      }
    }
    await permissionApi.assignRolePermissions(selectedRole.id, permissions);
    await permission.fetchPermissions();
    updateState({
      showPermissionAssign: false,
      selectedPermissions: null,
      selectedRole: null,
    });
  }, { showError: true, successMessage: t('translation:success') });

  const { selectedPermissions, selectedRole, showPermissionAssign } = state;
  return (
    <ListPage
      title={t('role_management')}
      apiPath="/v1/role"
      searchInfo={`${t('role_id')} ${t('translation:or')} ${t('title')}`}
      defaultState={{ sort: [{ orderBy: 'id', direction: 'ASC' }] }}
      createBoard={(onCreate, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          }}
          formItems={() => getFormItems()}
          onSubmit={(value) => {
            onCreate(value);
          }}
        />
      )}
      updateBoard={(selectedRecord, onUpdate, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          }}
          initialValues={selectedRecord}
          formItems={() => getFormItems()}
          onSubmit={(value, changedValue) => {
            onUpdate(selectedRecord.id, changedValue);
          }}
        />
      )}
      filter={(onFilterSubmit, onFilterClear, loading) => (
        <BaseForm
          loading={loading}
          formProps={{
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
          }}
          formItems={() => ([
            {
              // dataIndex: 'updatedAt',
              name: 'updatedAt',
              label: t('updated_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
            {
              // dataIndex: 'createdAt',
              name: 'createdAt',
              label: t('created_at'),
              render: () => (
                <RangePicker
                  allowClear
                />
              ),
            },
          ])}
          onSubmit={(value: any) => {
            const { updatedAt, createdAt } = value;
            const filterData: FilterData = { ...value };
            if (updatedAt?.length) {
              filterData.updatedAt = {
                from: updatedAt[0],
                to: updatedAt[1],
              };
            } else {
              delete filterData.updatedAt;
            }
            if (createdAt?.length) {
              filterData.createdAt = {
                from: createdAt[0],
                to: createdAt[1],
              };
            } else {
              delete filterData.createdAt;
            }
            onFilterSubmit(filterData);
          }}
          onClear={() => onFilterClear()}
        />
      )}
      columns={(showUpdate, onDelete, loading) => ([
        {
          title: t('role_id'),
          dataIndex: 'id',
          sorter: true,
        },
        {
          title: t('title'),
          dataIndex: 'title',
          sorter: true,
          render: (title) => title?.[currentLanguage]?.value,
        },
        {
          title: t('updated_at'),
          dataIndex: 'updatedAt',
          render: (updatedAt) => moment(updatedAt).format('YYYY-MM-DD HH:mm:ss'),
          sorter: true,
        },
        {
          title: t('created_at'),
          dataIndex: 'createdAt',
          render: (createdAt) => moment(createdAt).format('YYYY-MM-DD HH:mm:ss'),
          sorter: true,
        },
        {
          title: t('action'),
          render: (record) => (
            <Row gutter={10}>
              <Col>
                <Button
                  onClick={() => showUpdate(record)}
                >
                  <EditOutlined />
                </Button>

                <Button
                  onClick={() => onShowPermissionAssign(record)}
                >
                  {t('assign_permission')}
                </Button>
              </Col>
              <Col>
                <Popconfirm
                  title={t('translation:delete_record_confirm_msg')}
                  onConfirm={() => onDelete(record.id)}
                  okText={t('translation:yes')}
                  cancelText={t('translation:no')}
                >
                  <Button loading={loading}>
                    <DeleteOutlined />
                  </Button>
                </Popconfirm>
              </Col>
            </Row>
          ),
        },
      ])}
    >
      <Modal
        bodyStyle={{ padding: 0 }}
        visible={showPermissionAssign}
        footer={null}
        maskClosable={false}
        destroyOnClose
        onCancel={() => updateState({
          showPermissionAssign: false,
          selectedPermissions: null,
          selectedRole: null,
        })}
      >
        <Card title={t('permission_assigner')}>
          <PermissionAssigner
            loading={assignPermissionLoading}
            initialValues={selectedPermissions}
            onSubmit={(value) => onAssignPermission(selectedRole, value)}
          />
        </Card>
      </Modal>
    </ListPage>
  );
};

export default RoleManagement;
