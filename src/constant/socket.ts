/* eslint-disable import/prefer-default-export */
export const SOCKET_STATUS = {
  connected: 1,
  disconnected: 2,
  connecting: 3,
};
