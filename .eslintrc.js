const path = require('path');

module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'react',
    '@typescript-eslint',
  ],
  rules: {
    'react/react-in-jsx-scope': 'off',
    'react/jsx-filename-extension': [1, { extensions: ['.tsx', '.ts'] }],
    'jsx-a11y/anchor-is-valid': 'off',
    'max-len': 'off',
    'react/require-default-props': 'off',
    'import/extensions': 'off',
    'lines-between-class-members': 'off',
    'class-methods-use-this': 'off',
    'no-restricted-syntax': 'off',
    'object-curly-newline': 'off',
    'react/no-array-index-key': 'off',
    'jsx-a11y/click-events-have-key-events': 'off',
    'jsx-a11y/no-static-element-interactions': 'off',
    'react/jsx-props-no-spreading': 'off',
  },
  settings: {
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        jsx: 'never',
        ts: 'never',
        tsx: 'never',
      },
    ],
    'import/resolver': {
      alias: {
        map: [
          ['@utils', path.join(__dirname, './src/utils')],
          ['@pages', path.join(__dirname, './src/pages')],
          ['@components', path.join(__dirname, './src/components')],
          ['@constant', path.join(__dirname, './src/constant')],
          ['@interface', path.join(__dirname, './src/interface')],
          ['@public', path.join(__dirname, './src/public')],
        ],
        extensions: ['.tsx', '.ts', '.js', '.jsx', '.json'],
      },
    },
  },
};
