const cssLoaderConfig = require('@zeit/next-css/css-loader-config');
const antdTheme = require('./src/utils/theme/antd-theme');

module.exports = (nextConfig = {}) => ({ ...nextConfig,
  webpack: (config, options) => {
    const newOptions = { ...options };
    const newConfig = { ...config };
    if (!newOptions.defaultLoaders) {
      throw new Error(
        'This plugin is not compatible with Next.js versions below 5.0.0 https://err.sh/next-plugins/upgrade',
      );
    }

    const { dev, isServer } = newOptions;
    const {
      cssModules,
      cssLoaderOptions,
      postcssLoaderOptions,
      lessLoaderOptions = {
        modifyVars: antdTheme,
        javascriptEnabled: true,
      },
    } = nextConfig;

    newOptions.defaultLoaders.less = cssLoaderConfig(newConfig, {
      extensions: ['less'],
      cssModules,
      cssLoaderOptions,
      postcssLoaderOptions,
      dev,
      isServer,
      loaders: [
        {
          loader: 'less-loader',
          options: lessLoaderOptions,
        },
      ],
    });

    newConfig.module.rules.push({
      test: /\.less$/,
      exclude: /node_modules/,
      use: newOptions.defaultLoaders.less,
    });

    // disable antd css module
    newConfig.module.rules.push({
      test: /\.less$/,
      include: /node_modules/,
      use: cssLoaderConfig(newConfig, {
        extensions: ['less'],
        cssModules: false,
        cssLoaderOptions: {},
        dev,
        isServer,
        loaders: [
          {
            loader: 'less-loader',
            options: lessLoaderOptions,
          },
        ],
      }),
    });

    if (isServer) {
      const antStyles = /antd\/.*?\/style.*?/;
      const origExternals = [...newConfig.externals];
      newConfig.externals = [
        (context, request, callback) => {
          if (request.match(antStyles)) return callback();
          if (typeof origExternals[0] === 'function') {
            return origExternals[0](context, request, callback);
          }
          return callback();
        },
        ...(typeof origExternals[0] === 'function' ? [] : origExternals),
      ];

      newConfig.module.rules.unshift({
        test: antStyles,
        use: 'null-loader',
      });
    }

    if (typeof nextConfig.webpack === 'function') {
      return nextConfig.webpack(newConfig, newOptions);
    }

    return newConfig;
  } });
